#!/bin/bash

# --------------------------------------------------------------------------- #
  OUTDIR="_";TMP="TMPXX"
# --------------------------------------------------------------------------- #
  INKSCAPEEXTENSION="lib/python"
  export PYTHONPATH="$INKSCAPEEXTENSION"
  APPLYTRANSFORMPY="lib/python/applytransform.py"
  APPLYTRANSFORMPY=`realpath $APPLYTRANSFORMPY`
# --------------------------------------------------------------------------- #
  DEVMODE=`echo $* | sed 's/ /\n/g' | grep -- "^--dev" | wc -l`
   SVGSRC=`echo $* | sed 's/ /\n/g' | grep "\.[a-z]\{2,4\}$" | head -n 1`
# --------------------------------------------------------------------------- #
  IFSVG=`echo $SVGSRC | grep "\.svg$" | wc -l`
  if [ $IFSVG -lt 1 ]
  then echo -e "\e[31m\"$SVGSRC\"\e[0m not a svg file"; exit 0; fi
# ----
  IFHTTP=`echo $SVGSRC | grep "http.\?://" | wc -l`
  if [ $IFHTTP -ge 1 ]
  then RESPONSE=`curl -s -o /dev/null \
                 -IL -w "%{http_code}" $SVGSRC`
       if [ $RESPONSE == '200' ]; then
            wget --no-check-certificate     \
                 -O ${TMP}.svg $SVGSRC 2>&1 > /dev/null 2>&1
       else echo -e "\e[31m$SVGSRC\e[0m does not exist!"
            exit 0;
       fi
       if [ "$DEVMODE" -gt 0 ]
       then  echo "DEVMODE!"
             if [ `ls $OUTDIR/dev* 2>/dev/null | #
                        wc -l` -gt 0 ]
                  then  rm $OUTDIR/dev*
             fi
       fi
  else if [ "$DEVMODE" -lt 1 ]
       then  echo -e "\e[31mPLEASE PROVIDE URL!\e[0m"; exit 0; 
       else  echo "DEVMODE!"
             if [ -f "$SVGSRC" ]
             then if [ `ls $OUTDIR/dev* 2>/dev/null | #
                        wc -l` -gt 0 ]
                  then  rm $OUTDIR/dev*
                  fi;   cp $SVGSRC ${TMP}.svg
             else echo -e "\e[31m$SVGSRC\e[0m does not exist!"; exit 0;
             fi
       fi
  fi
# --------------------------------------------------------------------------- #
# FUNCTIONS
# --------------------------------------------------------------------------- #
  mkseed(){ openssl enc -aes-256-ctr -pass pass:"$1" \
            -nosalt </dev/zero 2>/dev/null; }
  # USAGE: shuf -n 1 --random-source=<(mkseed $SRCNAME)
# --------------------------------------------------------------------------- #
  SRCNAME=`basename $SVGSRC`
  SRCCONF=`grep $SRCNAME src.conf | #
           cut -d "|" -f 2- | head -n 1`
  XPORTID=`echo $SRCNAME       | # DISPLAY BASENAME
           md5sum | cut -c 1-10` # GENERATE/CUT HASH
  if [ "$DEVMODE" -gt 0 ];then XPORTID="dev"; fi
  CSSOUT="${OUTDIR}/${XPORTID}.css"
  if [ -f "$CSSOUT" ]; then rm $CSSOUT; fi
  NTHMAX="1";CSSBGIMG="background-image:url("
  UIREF="$OUTDIR/${XPORTID}_010000_0000000.svg"
# =========================================================================== #
# DO IT NOW!
# =========================================================================== #
  SVG=${TMP}.svg
# --------------------------------------------------------------------------- #
# MOVE ALL LAYERS ON SEPARATE LINES IN A TMP FILE
# --------------------------------------------------------------------------- #
  sed ':a;N;$!ba;s/\n//g' $SVG           | # REMOVE ALL LINEBREAKS
  sed 's/<g/\n&/g'                       | # MOVE GROUP TO NEW LINES
  sed '/groupmode="layer"/s/<g/4Fgt7R/g' | # PLACEHOLDER FOR LAYERGROUP OPEN
  sed ':a;N;$!ba;s/\n/ /g'               | # REMOVE ALL LINEBREAKS
  sed 's/4Fgt7R/\n<g/g'                  | # RESTORE LAYERGROUP OPEN + NEWLINE
  sed 's/display:none/display:inline/g'  | # MAKE VISIBLE EVEN WHEN HIDDEN
  sed 's/<\/svg>/\n&/g'                  | # CLOSE TAG ON SEPARATE LINE
  sed "s/^[ \t]*//"                      | # REMOVE LEADING BLANKS
  tr -s ' '                              | # REMOVE CONSECUTIVE BLANKS
  tee > ${SVG%%.*}.tmp                     # WRITE TO TEMPORARY FILE
# --------------------------------------------------------------------------- #
# GET/SET CONFIG
# --------------------------------------------------------------------------- #
  SVGW=`sed 's/ /\n/g' $SVG | grep '^width='  | head -n 1 | cut -d '"' -f 2`
  SVGH=`sed 's/ /\n/g' $SVG | grep '^height=' | head -n 1 | cut -d '"' -f 2`
# ----
  COLOR=`echo $SRCCONF  | sed 's/;/\n/g' | #
         grep "^color:" | cut -d ":" -f 2 | sed '/[^#0-9a-f]\+/d'` #
  WHITE=`echo $SRCCONF  | sed 's/;/\n/g' | #
         grep "^white:" | cut -d ":" -f 2 | sed '/[^#0-9a-f]\+/d'` #
  BLACKFILL=`echo $SRCCONF  | sed 's/;/\n/g' | #
             grep "^blackfill:" | cut -d ":" -f 2 | sed '/[^#0-9a-f]\+/d'` #
  STROKESCALE=`echo $SRCCONF  | sed 's/;/\n/g' | #
               grep "^stroke:" | cut -d ":" -f 2 | sed '/[^0-9\.]\+/d'` #
  EXTEND=`echo $SRCCONF      | #
          sed 's/;/\n/g'     | #
          grep "^extend:"    | #
          cut -d ":" -f 2    | #
          sed '/[^0-9\.]\+/d'` #
# --------------------------------------------------------------------------- #
# CREATE VALUES FOR EXTENDED CANVAS
# --------------------------------------------------------------------------- #
  if [ "$EXTEND" != "" ]
   then XSHIFT=`python -c "print ($SVGW * ($EXTEND - 1)) / 2"`
        YSHIFT=`python -c "print ($SVGH * ($EXTEND - 1)) / 2"`
          SVGW=`python -c "print $SVGW * $EXTEND"`
          SVGH=`python -c "print $SVGH * $EXTEND"`
     TRANSFORM=" transform=\"translate($XSHIFT,$YSHIFT)\""
   else EXTEND=1
  fi
# --------------------------------------------------------------------------- #
# WRITE ONE SVG PER LAYER (CONSIDER LAYER-ORDER/Z-POSITIONING)
# --------------------------------------------------------------------------- #
 (IFS=$'\n';C=00100001
  for LINE in `grep "^<g" ${SVG%%.*}.tmp`
   do
      LAYERNAME=`echo $LINE | sed 's/</\n</g'    | #
                 sed  '/^<g/s/pe:label/\nlabel/' | # PUT NAME LABEL ON NL 
                 grep '^label' | cut -d "\"" -f 2` # EXTRACT NAME 

      if [ "$LAYERNAME" == "XX_UIREF" ] ||
         [ `echo $LAYERNAME | grep -v "XX_" | wc -l` -gt 0 ]
      then

       TYPEHASH=`echo $LAYERNAME      | # $LAYERNAME
                 sed 's/[-_][0-9]*$//'| # RM NUMBER
                 md5sum | cut -c 1-4`   # MAKE HASH
        TYPENUM=`echo $LAYERNAME                        | # $LAYERNAME
                 sed 's/\(.*\)\([-_]\)\([0-9]*$\)/\3/'  | # KEEP NUMBER
                 md5sum | sed 's/[^0-9]//g' | cut -c 1-3` # NUM ONLY HASH

      LAYERHASH="$TYPEHASH$TYPENUM"

      # if [ `echo "$LAYERHASHCOLLECT" | sed 's/,/\n/g' | #
      #       grep $LAYERHASH | wc -l` -gt 0 ]
      # then #echo "DUPLICATE ($LAYERNAME/$LAYERHASH)"
      #       echo "$LAYERHASHCOLLECT" | #
      #       sed 's/,/\n/g' | grep $LAYERHASH
      # fi
      #LAYERHASHCOLLECT="${LAYERHASHCOLLECT},${LAYERNAME}:$LAYERHASH"

      if [ "$LAYERNAME" == "XX_UIREF" ]
      then SVGOUT="$UIREF"
      else PADC=`echo 000000$C | rev | cut -c 1-8 | rev`
           SVGOUT="$OUTDIR/${XPORTID}_${PADC}_${LAYERHASH}.svg"
           C=`expr $C + 1`
      fi

      LAYERZPOS=`echo $LINE | sed 's/>/>\n/g' | head -n 1 | #
                 sed 's/ /\n/g' | grep "^zpos=" | cut -d '"' -f 2`
      if [ "$LAYERZPOS" != "" ]
      then if [ `echo $LAYERZPOS | grep "^-" | wc -l` -gt 0 ]
           then LAYERZPOS=`echo $LAYERZPOS | sed 's/[^0-9]//g'`
                ZPOS=`expr $C - $((LAYERZPOS * 100))`
                PADZ=`echo 000000${ZPOS} | rev | cut -c 1-8 | rev`
           else LAYERZPOS=`echo $LAYERZPOS | sed 's/[^0-9]//g'`
                ZPOS=`expr $C + $((LAYERZPOS * 100))`
                PADZ=`echo 000000${ZPOS} | rev | cut -c 1-8 | rev`
          fi
          SVGOUT="$OUTDIR/${XPORTID}_${PADZ}_${LAYERHASH}.svg"
      fi

        HASSUBZPOS=`echo $LINE | sed 's/>[ ]*/&\n/g'   | #
                    egrep -v '^<[/]*g' | grep 'zpos="' | #
                    wc -l`
      # ---------------------------------------------------------------- #
        if [ "$HASSUBZPOS" -gt 0 ];then
      # ---------------------------------------------------------------- #
        head -n 1 ${SVG%%.*}.tmp | #
        sed "s/width=\"[0-9]*\"/width=\"$SVGW\"/" | #
        sed "s/height=\"[0-9]*\"/height=\"$SVGH\"/"           >  $SVGOUT
        echo "<g$TRANSFORM>"                                  >> $SVGOUT
        echo "$LINE"                      | # DISPLAY THIS LINE
        sed 's/>[ ]*/&\n/g'               | # PUT NEWLINE AFTER '>'
        sed '/^<[/]*g/!s/^.*zpos=".*$//g' | # DELETE IF NOT <G> AND HAS ZPOS
        tee                                                   >> $SVGOUT
        echo "</g>"                                           >> $SVGOUT
        echo "</svg>"                                         >> $SVGOUT
        C=`expr $C + 1`
      # ----
        for SUBZPOS in `echo $LINE           | #
                        sed 's/>[ ]*/&\n/g'  | egrep -v '^<[/]*g' | #
                        sed 's/zpos="/\n&/g' | grep '^zpos="'     | #
                        cut -d '"' -f 2      | sort -un`
         do
            ZPOSSELECT="$SUBZPOS"
            if [ `echo $SUBZPOS | grep "^-" | wc -l` -gt 0 ]
            then SUBZPOS=`echo $SUBZPOS | sed 's/[^0-9]//g'`
                 ZPOS=`expr $C - $((SUBZPOS * 100))`
                 PADZ=`echo 000000${ZPOS} | rev | cut -c 1-8 | rev`
            else SUBZPOS=`echo $SUBZPOS | sed 's/[^0-9]//g'`
                 ZPOS=`expr $C + $((SUBZPOS * 100))`
                 PADZ=`echo 000000${ZPOS} | rev | cut -c 1-8 | rev`
            fi
            SVGOUT="$OUTDIR/${XPORTID}_${PADZ}_${LAYERHASH}.svg"

            head -n 1 ${SVG%%.*}.tmp | #
            sed "s/width=\"[0-9]*\"/width=\"$SVGW\"/" | #
            sed "s/height=\"[0-9]*\"/height=\"$SVGH\"/"       >  $SVGOUT
            echo "<g$TRANSFORM>"                              >> $SVGOUT
            echo $LINE                           | # DISPLAY THIS LINE
            sed 's/>[ ]*/&\n/g'                  | # PUT NEWLINE AFTER '>'
            egrep "^<[/]*g|zpos=\"$ZPOSSELECT\"" | # SELCT <G> AND THIS ZPOS
            tee                                               >> $SVGOUT
            echo "</g>"                                       >> $SVGOUT
            echo "</svg>"                                     >> $SVGOUT
        done
      # ---------------------------------------------------------------- #
        else
      # ---------------------------------------------------------------- #
        head -n 1 ${SVG%%.*}.tmp | #
        sed "s/width=\"[0-9]*\"/width=\"$SVGW\"/" | #
        sed "s/height=\"[0-9]*\"/height=\"$SVGH\"/"           >  $SVGOUT
        echo "<g$TRANSFORM>"                                  >> $SVGOUT
        echo "$LINE"                                          >> $SVGOUT
        echo "</g>"                                           >> $SVGOUT
        echo "</svg>"                                         >> $SVGOUT
      # ---------------------------------------------------------------- #
        fi
      # ---------------------------------------------------------------- #
      fi
  done;)
# --------------------------------------------------------------------------- #
# MODIFY/OPTIMIZE ALL SVG FILES
# --------------------------------------------------------------------------- #
  for SVGOUT in `ls $OUTDIR/${XPORTID}_*.svg`
   do
     # ---------------------------------------------------------------- #
       if [ "$STROKESCALE" != "" ]
       then
        for S in `sed 's/stroke-width/\n&/g' $SVGOUT | #
                  sed 's/ /\n/g' | #
                  grep '^stroke-width' | cut -d ":" -f 2 | #
                  sed 's/;.*//' | sort -u | sed 's/[^0-9\.]//g' | #
                  awk '{ print length($0) " " $0; }' | #
                  sort -r -n | cut -d ' ' -f 2-`
        do
         NEWSTROKE=`python -c "print round($S * $STROKESCALE,2)"`
         sed -i "s/stroke-width:$S/stroke-width:XX$NEWSTROKE/g" $SVGOUT
        done
        sed -i 's/stroke-width:XX/stroke-width:/g' $SVGOUT
       fi
     # ---------------------------------------------------------------- #
       PROTECTBLACK="XXXX${RANDOM}";PROTECTWHITE="XXXX${RANDOM}"
     # ---------------------------------------------------------------- #
       sed -i "s/#[0]\{6\}\b/$PROTECTBLACK/g"                 ${SVGOUT}
     # ---------------------------------------------------------------- #
       if [ "$COLOR" != "" ]
       then sed -i "s/#[Ff]\{6\}\b/$PROTECTWHITE/g"           ${SVGOUT}
            sed -i "s/#[a-fA-F0-9]\{6\}\b/$COLOR/g"           ${SVGOUT}
            sed -i "s/$PROTECTWHITE/#ffffff/g"                ${SVGOUT}
       fi
     # ---------------------------------------------------------------- #
       if [ "$WHITE" != "" ]
       then sed -i "s/#[Ff]\{6\}\b/$WHITE/g"                  ${SVGOUT}
       fi
     # ---------------------------------------------------------------- #
       if [ "$BLACKFILL" != "" ]
       then sed -i "s/fill:$PROTECTBLACK\b/fill:$BLACKFILL/g" ${SVGOUT}
       else sed -i "s/fill:$PROTECTBLACK\b/fill:#000000/g"    ${SVGOUT}
       fi
     # ---------------------------------------------------------------- #
       if [ "$COLOR" != "" ]
       then sed -i "s/$PROTECTBLACK/$COLOR/g"                 ${SVGOUT}
       else sed -i "s/$PROTECTBLACK/#000000/g"                ${SVGOUT}
       fi
     # ---------------------------------------------------------------- #
       python3 $APPLYTRANSFORMPY ${SVGOUT} | # APPLY TRANSFORMATIONS
       sed 's/zpos="[^"]*"//'              | # RM ZPOS
       sed 's/<\/*g[^>]*>//g'              | # RM GROUPS
       tee > ${SVGOUT}.tmp                   # WRITE TO TMP FILE
       scour -q --remove-metadata        \
             --shorten-ids --indent=none \
             --disable-style-to-xml      \
             --strip-xml-prolog          \
             --enable-id-stripping       \
             --enable-comment-stripping  \
             -i ${SVGOUT}.tmp -o ${SVGOUT}
       sed -i 's/xmlns:[^=]*="[^"]*"//g' $SVGOUT
       sed -i 's/ \+/ /g'                $SVGOUT
       sed -i '/^<!--.*-->$/d'           $SVGOUT
       rm ${SVGOUT}.tmp
     # ---------------------------------------------------------------- #
  done
# --------------------------------------------------------------------------- #
# START CSS
# --------------------------------------------------------------------------- #
  NUMTYPES=`ls $OUTDIR/${XPORTID}_*              | # LIST FILES
            grep -v "$UIREF"                     | # IGNORE UIREF
            sed "s,$OUTDIR/${XPORTID}_[0-9]*_,," | # RM NAME BASE
            cut -c 1-4                           | # RM ALL BUT NUMBERS 
            sort -u | wc -l`                       # UNIQ + COUNT
  echo "/* SVGSRC: $SVGSRC */"                                     >  $CSSOUT
  echo "/* NUMYTPES: $NUMTYPES */"                                 >> $CSSOUT
  echo ""                                                          >> $CSSOUT
# --------------------------------------------------------------------------- #
# WRITE REFERENCE (IF EXISTS)
# --------------------------------------------------------------------------- #
  if [ -f $UIREF ]
  then UIREF=`basename $UIREF`
       echo "div.ref > div { background-color:transparent;"        >> $CSSOUT
       echo "                background-image: url(\"$UIREF\"); }" >> $CSSOUT
       echo ""                                                     >> $CSSOUT
  fi
# --------------------------------------------------------------------------- #
# HANDLE SVG SIZES
# --------------------------------------------------------------------------- #
  MAXW=`echo $SRCCONF      | #
        sed 's/;/\n/g'     | #
        grep "^max-width:" | #
        cut -d ":" -f 2    | #
        sed '/[^0-9]\+/d'`   #

  if [ "$MAXW" != "" ]
  then  MAXW=`python -c "print $MAXW * $EXTEND"`
       SCALE=`python -c "print $SVGW / ${MAXW}.0"`
        MAXH=`python -c "print $SVGH / $SCALE" | cut -d "." -f 1`
        MAXW="${MAXW}px";MAXH="${MAXH}px";
  else  MAXW="none";     MAXH="none";
  fi
# ----
  MINW=`echo $SRCCONF      | #
        sed 's/;/\n/g'     | #
        grep "^min-width:" | #
        cut -d ":" -f 2    | #
        sed '/[^0-9]\+/d'`   #

  if [ "$MINW" != "" ]
  then  MINW=`python -c "print $MINW * $EXTEND"`
       SCALE=`python -c "print $SVGW / ${MINW}.0"`
        MINH=`python -c "print $SVGH / $SCALE" | cut -d "." -f 1`
        MINW="${MINW}px";MINH="${MINH}px";
  else  MINW="0";        MINH="0";
  fi
# ----
  SIZE=`echo $SRCCONF      | #
        sed 's/;/\n/g'     | #
        grep "^size:"      | #
        cut -d ":" -f 2    | #
        sed '/[^0-9\.]\+/d'` #
  if [ "$SIZE" != "" ]
  then SIZE=`python -c "print $SIZE * $EXTEND"`
  else SIZE="1"
  fi
# --------------------------------------------------------------------------- #
  for VIEWPORT in 900 800 700 600 500 400
   do SCALE=`python -c "print round($SVGW / ${VIEWPORT}.0,3)"`
      TARGETW=`python -c "print ($SVGW / $SCALE - 100) * $SIZE" | #
               cut -d "." -f 1`
      TARGETH=`python -c "print ($SVGH / $SCALE - 100) * $SIZE" | #
               cut -d "." -f 1`
      if [ "$VIEWPORT" == "900" ];then # DEFINE STANDARDS ON FIRST RUN
            echo "div.ref,
                  div.element { width:${TARGETW}px;height:${TARGETH}px;
                                max-width:$MAXW;max-height:$MAXH;
                                min-width:$MINW;min-height:$MINH; }"  | #
            sed ':a;N;$!ba;s/\n//g' | tr -s ' '                    >> $CSSOUT
      fi
      echo "@media screen and (max-width:${VIEWPORT}px){
            div.ref,
            div.element { width:${TARGETW}px;height:${TARGETH}px; }}" | #
      sed ':a;N;$!ba;s/\n//g' | tr -s ' '                          >> $CSSOUT
  done
  echo ""                                                          >> $CSSOUT
# --------------------------------------------------------------------------- #
# CREATE CSS FOR SEPARATE ELEMENTS
# --------------------------------------------------------------------------- #
  C1=1
  for TYPE in `ls $OUTDIR/${XPORTID}_*              | # LIST FILES
               grep -v "$UIREF"                     | # IGNORE UIREF
               sed "s,$OUTDIR/${XPORTID}_[0-9]*_,," | # RM NAME BASE
               cut -c 1-4 | sort -u`                  # REDUCE TO TYPEHASH + UNIQ
   do  TYPEID="e$C1";CSSTYPE="div.${TYPEID}"
       C2=1;for ELEMENTNUM in `ls $OUTDIR/${XPORTID}*svg   | # LIST FILES
                               grep -v "$UIREF"            | # IGNORE UIREF
                               grep $TYPE                  | # SELECT TYPE 
                               rev | cut -d "_" -f 1 | rev | # SELECT LAST FIELD
                               cut -d "." -f 1             | # RM EXTENSION
                               cut -c 5-                   | # REDUCE TO TYPE NUM
                               sort -u`                      # UNIQ
            do   # ---- 
            C3=1;for PART in `ls $OUTDIR/${XPORTID}*svg   | # LIST FILES
                              grep -v "$UIREF"            | # IGNORE UIREF
                              grep "${TYPE}${ELEMENTNUM}" | # SELECT MATCHING
                              rev | cut -d "/" -f 1 | rev`  # GET BASENAME
                 do # ----
                     CSSPRE="$CSSTYPE > .r${C2}:nth-of-type(${C3})"
                     ZINDEX=`basename $PART | cut -d "_" -f 2`
                     CSS="${CSS}::NL::"`echo "$CSSPRE { $CSSBGIMG\"$PART\");\
                                                         z-index:$ZINDEX;}" | #
                                 sed ':a;N;$!ba;s/\n/ /g' | tr -s ' '`
                     if [ $C3 -gt $NTHMAX ];then NTHMAX=$C3;fi
                     C3=`expr $C3 + 1`
                 done # ----
                 echo "$CSS" >> ${TMP}.${TYPE}
                 CSS="";C2=`expr $C2 + 1` # RESET/PROCEED
       done # ----
       EMAX="${EMAX},\""`expr $C2 - 1`"\""
     # -------------------------------------------------------------------- #
     # WRITE CSS, CLEAN UP, PROCEED
     # -------------------------------------------------------------------- #
       sed 's/^[ ]*//' ${TMP}.${TYPE} >> $CSSOUT
       rm ${TMP}.${TYPE}
       C1=`expr $C1 + 1`
  done

  EMAX=`echo $EMAX | sed 's/^,//'`
  sed -i "s/NTHMAXFOO/$NTHMAX/" $CSSOUT # SUBSTITUTE PLACEHOLDER
  sed -i "s/::NL::/\n/g"        $CSSOUT # INJECT LINEBREAKS
  sed -i "/^[ ]*$/d"            $CSSOUT # RM EMPTY LINES

# --------------------------------------------------------------------------- #
# WRITE HTML (IF DEV MODE)
# --------------------------------------------------------------------------- #
  if [ "$DEVMODE" -gt 0 ];then
        HTML="dev.html";SEEDSTART=`echo $RANDOM              | #
                                   sed 's/^.*$/&&&&&&&&&&/g' | #
                                   sed 's/^.*$/&&&&&&&&&&/g' | #
                                   cut -c 1-$((NUMTYPES * 3))` #
        echo '<html><head>'                                          >  $HTML
        for JS in jquery.min.js jquery.md5.js \
                  jquery.mousewheel.js        \
                  seedrandom.js hammer.js
         do echo "<script type=\"text/javascript\" \
                          charset=\"utf-8\" \
                          src=\"js/$JS\"></script>" | tr -s ' '      >> $HTML
        done
        echo '<style>@import "_/dev.css"</style>'                    >> $HTML
        echo '<style>@import "css/kombinator.css"</style>'           >> $HTML
        echo '</head><body>'                                         >> $HTML
        echo '<div id="touchthis"></div>'                            >> $HTML
        echo '<div class="wrapper switch">'                          >> $HTML
        for N in `seq $NTHMAX`;do EI="<div></div>$EI";done
        for E in `seq 1 $NUMTYPES`
         do echo "<div class=\"element e$E\">$EI</div>"              >> $HTML
        done  
        echo '</div>'                                                >> $HTML
        echo '<div class="wrapper fixed">'                           >> $HTML
        echo '<div class="ref"><div></div></div>'                    >> $HTML
        echo '</div>'                                                >> $HTML
        echo '<script type="text/javascript">'                       >> $HTML
        echo ' var eID = "dev";'                                     >> $HTML
        echo " var eNum = \"$NUMTYPES\";"                            >> $HTML
        echo ' var clientID = "dev";'                                >> $HTML
        echo " var globalSeed = \"$SEEDSTART\";"                     >> $HTML
        echo " var eMax = [ "$EMAX" ]"                               >> $HTML
        echo '</script>'                                             >> $HTML
        echo "<script type=\"text/javascript\"  \
                      charset=\"utf-8\"         \
                      src=\"js/kombinator.js\"> \
              </script>" | tr -s ' '                                 >> $HTML
        echo '<script type="text/javascript">'                       >> $HTML
        echo '$(document).ready(function(){'                         >> $HTML
        echo "  switchGrafik(globalSeed,'');"                        >> $HTML
        echo '});'                                                   >> $HTML
        echo '</script>'                                             >> $HTML
        echo '</body></html>'                                        >> $HTML
  fi
# --------------------------------------------------------------------------- #
# DISPLAY URL TO UI
# --------------------------------------------------------------------------- #
  echo "http://"`hostname -I | sed 's/ //g'`":8000/index.php?src=$XPORTID"
# --------------------------------------------------------------------------- #
# CLEAN UP (MAKE SURE $TMPID IS SET FOR WILDCARD DELETE)
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi
# --------------------------------------------------------------------------- #

exit 0;
