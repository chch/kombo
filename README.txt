
 K O M B O
 ---------
 
 This is interface utility to visually check
 possible permutations of a svg file.

 The svg is prepared with different elements
 on different layers identified by name_number
 e.g.

   A-001 or A-002 or A-003 or A-004
 + B-001 or B-002
 + C-001 or C-002 or C-003 or C-004



 R E Q U I R E M E N T S
 -----------------------
 TODO!


 H O W T O
 ---------

# GENERATE FILES TO RUN VIA PHP
 :$ ./mkui.sh  http://url.to/file.svg

# GENERATE VIEW-ONLY PREVIEW (=html)
 :$ ./mkui.sh --dev path/to/file.svg


 RUN UI VIA LOCAL WEBSERVER
 --------------------------
(:$ sudo aptitude install php-cli)
#:$ php -S 127.0.0.1:8000 -t .

 SERVE TO THE NETWORK
 --------------------
# FIND OUT YOUR IP ADRESS 
 :$ hostname -I

# WRITE IP ADRESS TO ip.conf

# RUN WEBSERVER WITH IP FROM ip.conf
 :$ php -S `cat ip.conf` -t .

