<?php   // GLOBALS
        // ------------------------------------------------------------- //
           $ipconf = "ip.conf";
           if ( file_exists($ipconf) ) {
                $baseUrl = "http://" . trim(file($ipconf)[0]);
           } else {
                $baseUrl = "http://localhost:8000";
          }
          $uiPath     = "_";
          $grafikPath = "o";
          $stampPath  = "s";
          $cssPath    = "css";

          $nfoFile     = "src.conf";

          $loadingIcon = $uiPath . "/" . "loading.svg";
          $pendingIcon = $uiPath . "/" . "pending.svg";
          $gotoIcon = $uiPath . "/" . "goto.svg";
          $gotoIconHover = $uiPath . "/" . "goto_HOVER.svg";
          $shorturlIcon = $uiPath . "/" . "shorturl.svg";
          $shorturlIconHover = $uiPath . "/" . "shorturl_HOVER.svg";

          $fields  = '^([0-9a-f]*)\|'    . // $1 = checksum     
                     '([0-9a-f]{10})\|'  . // $2 = srcbase
                     '([0-9a-f|]+)\|'    . // $3 = layers
                     '([0-9]{3,})\|'     . // $4 = seed
                     '([0-9]{13})$';       // $5 = timestamp

?><?php // ----------------------------------------------------------------- //
           $requestid = isset($_GET['id'])?strip_tags(trim($_GET['id'])):'';
           $regMatch  = "[0-9a-f]"; // SAVED ONLY (NOT MARKED)
        // ----------------------------------------------------------------- //
        // CREATE ARRAY WITH ALL SAVED FILES (UNIFIED BY HASH)
        // ----------------------------------------------------------------- //
           $saved = array();
           foreach (glob($stampPath."/".$regMatch."*.list") as $listPath) {

              $list = array_values(array_filter(file($listPath),"trim"));

              // CREATE ARRAY FOR EACH HASH (=KEY)
              // OVERWRITE ITEM => ONE ENTRY FOR EACH HASH

              foreach($list as $line) {

               $name   = trim(preg_replace("/" . $fields . "/","$2",$line));
               $hash   = trim(preg_replace("/" . $fields . "/","$1",$line));
               $seed   = trim(preg_replace("/" . $fields . "/","$4",$line));
               $layers = trim(preg_replace("/" . $fields . "/","$3",$line));
               $time   = substr($line,-14,13);

               $saved[$hash] = array('hash'   => $hash,
                                     'name'   => $name, 
                                     'layers' => $layers,
                                     'seed'   => $seed,
                                     'time'   => $time);
              }
            }
        // ----------------------------------------------------------------- //
        // CREATE ARRAY WITH LAST SEEDS
        // ----------------------------------------------------------------- //
           $last = array();$c=0;
           foreach (glob($stampPath."/*.list") as $listPath) {

              $list = array_values(array_filter(file($listPath),"trim"));

              foreach($list as $line) {

               $hash = trim(preg_replace("/" . $fields . "/","$1",$line));
               $seed = trim(preg_replace("/" . $fields . "/","$4",$line));
               $time = substr($line,-14,13) . str_pad($c,10,"0",STR_PAD_LEFT);
               $last[$time] = array('hash'   => $hash,
                                    'seed'   => $seed,
                                    'time'   => $time);
               $c++;
              }
            }
        // ----------------------------------------------------------------- //
           ksort($last,SORT_NUMERIC);
        // ----------------------------------------------------------------- //
            $seeds = array();
            foreach($last as $item) {
               $hash   = $item['hash'];
               $seed   = $item['seed'];
               $seeds[$hash] = array('seed' => $seed);
            }
        // ----------------------------------------------------------------- //
        // CREATE ARRAY TO SHOW 
        // ----------------------------------------------------------------- //
           $show = array();$c=0;
           foreach($saved as $key => $item) {

                  $hash   = $item['hash'];
                  $name   = $item['name'];
                  $layers = $item['layers'];
                  $seed   = $item['seed'];
                  $time   = $item['time'];

                $lastseed = $seeds[$hash]['seed'];
                $id       = substr(
                            preg_replace("/[^b-e4-8]/","",
                            md5($layers)),0,8);
                $hrefid   = substr(md5($id),0,5);

                    $key  = $time . str_pad($c,10,"0",STR_PAD_LEFT);
              $show[$key] = array('hash'     => $hash,
                                  'name'     => $name, 
                                  'layers'   => $layers,
                                  'seed'     => $seed,
                                  'lastseed' => $lastseed,
                                  'id'       => $id,
                                  'hrefid'   => $hrefid,
                                  'time'     => $time);

              $c++;if ( $hrefid == $requestid ) { $srcName = $name; }
           }
        // ----------------------------------------------------------------- //
           ksort($show,SORT_NUMERIC);
        // ----------------------------------------------------------------- //
           if ( isset($srcName) ) { 

              if ( file_exists($nfoFile)) {

                   $srcInfo   = file($nfoFile);
                   $srcMatch  = array_values(
                                preg_grep("/^".$srcName."/",$srcInfo))[0];
                   $htmlTitle = preg_replace('/^[^:]*:[^:]*:/','',$srcMatch);
              }
           } 

           if ( !isset($htmlTitle) ) { $htmlTitle = "kombo/ALL"; }
        // ----------------------------------------------------------------- //
        // CREATE ARRAY WITH SVG FILES 
        // ----------------------------------------------------------------- //
           $svgFiles = array();
           foreach (glob($grafikPath."/*.svg") as $svgFile) {

                    $srcID = substr(basename($svgFile),0,13);
                    $svgFiles[$srcID] = basename($svgFile);
           }
        // ----------------------------------------------------------------- //
        // CREATE ARRAY WITH MAIN+SRC IDs
        // ----------------------------------------------------------------- //
           $srcIDs = array();
           foreach (glob($grafikPath."/*.gif") as $gifFile) {

                       $iD = substr(basename($gifFile),14,8);         
                    $srcID = substr(basename($gifFile),0,13);
                    $srcIDs[$iD] = $srcID;
           }
        // ----------------------------------------------------------------- //
        // SHOW PLAIN LIST        
        // ----------------------------------------------------------------- //
           if (isset( $_GET['plain'])){
             if (isset($_SERVER['HTTP_USER_AGENT'])
                 && preg_match('/^(curl|wget)/i',
                   $_SERVER['HTTP_USER_AGENT'])) {
                     $isCurl = TRUE;
             } else {
             echo "<html><head>";
             echo '<meta name="robots" content="noindex" />';
             echo "</head><pre>" . "\n";
             }
             $br = "";
             foreach ($show as $key => $item) {
                $id = $item['id'];$name = $item['name'];
                if ( ($srcName == "") || ($name == $srcName) ) {
                  if ( isset($srcIDs[$id]) ) { $srcID = $srcIDs[$id]; 
                                               $svgName = $svgFiles[$srcID];
                                               $svgMatch = $grafikPath . 
                                                           "/" . $svgName;
                  } else { $svgMatch=""; } 
                  $svgMatch = (!empty(glob($svgMatch)[0])) 
                               ? glob($svgMatch)[0] : false;
                  if ( strlen($svgMatch) > 1 ) { 
                       $svgFile = $baseUrl."/".$svgMatch;
                       if ( $isCurl == TRUE ) {
                       echo $br
                            . $svgFile;
                       } else {
                       echo $br
                            . '<a href="'
                            . $svgFile
                            . '">'
                            . $svgFile
                            . '</a>';
                       }
                       $br = "\n";
                  } 
                }
             }
            if ( $isCurl != TRUE ) { echo "\n" . "</pre></html>"; }
            exit(0);
           }
        // ----------------------------------------------------------------- //
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head><meta charset="utf-8"/>
<?php echo '<title>' . $htmlTitle . '</title>'; ?>
<meta name="viewport" content="width=device-width,user-scalable=no,
                               initial-scale=0.9,maximum-scale=0.9,
                               minimum-scale=0.9" />
<?php foreach (glob($cssPath."/list*.css") as $cssFile) {
      echo '<link rel="stylesheet" type="text/css" media="all" href="'.$cssFile.'">'."\n";
      }
?>
<style>
<?php if (file_exists($shorturlIcon)) {
      echo 'figcaption.href div.shorturl { background-image:url("'.$shorturlIcon.'");}'."\n";
      }
      if (file_exists($shorturlIconHover)) {
      echo 'figcaption.href div.shorturl:hover { background-image:url("'.$shorturlIconHover.'");}'."\n";
      }
      if (file_exists($gotoIcon)) {
      echo 'figcaption.href div.goto { background-image:url("'.$gotoIcon.'");}'."\n";
      }
      if (file_exists($gotoIconHover)) {
      echo 'figcaption.href div.goto:hover { background-image:url("'.$gotoIconHover.'");}'."\n";
      }
?>
figcaption.href div.goto { display:block!important; } </style>
</head>
<body>
<?php   // CREATE IMAGE LIST
        // ------------------------------------------------------------- //

         foreach ($show as $key => $item) {

          $hash     = $item['hash'];
          $name     = $item['name'];
          $layers   = $item['layers'];
          $seed     = $item['seed'];
          $time     = $item['time'];
          $lastseed = $item['lastseed'];
          $id       = $item['id'];
          $hrefid   = $item['hrefid'];

             if ( ($srcName == "") ||
                  ($name == $srcName) ) {

                if ( isset($srcIDs[$id]) ) { 

                   $srcID = $srcIDs[$id]; 
                   $svgName = $svgFiles[$srcID];
                   $svgMatch = $grafikPath . "/" . $svgName;
                   $thumbName  = basename($svgName, '.svg') 
                                 . "_*" . ".gif";
                   $thumbMatch = $grafikPath . "/" . $thumbName;

                } else { $svgMatch="";$thumbMatch=""; }

                $svgMatch = (!empty(glob($svgMatch)[0])) 
                             ? glob($svgMatch)[0] : false;
                $thumbMatch = (!empty(glob($thumbMatch)[0])) 
                               ? glob($thumbMatch)[0] : false;
    
                if ( $hrefid == $requestid ) {
                     echo '<figure class="a target" id="'.$hrefid.'">';
                } else {
                     echo '<figure class="a" id="'.$hrefid.'">';
                }    

                echo '<figure class="b ' . $name  .'">';
                echo '<figure class="center">';
     
                if ( strlen($thumbMatch) > 1 ) {
    
                //if (file_exists($loadingIcon)) {
                //echo '<img src="'.$loadingIcon.'"';
                //} else {
                //echo '<img src="data:image/gif;base64,R0lGODlhAQABAIABAMv';
                //echo           'Ly////yH5BAEAAAEALAAAAAABAAEAAAICRAEAOw=="';
                //}
                //echo ' data-src="' . $thumbMatch  . '" class="unveil"/>';
                //echo '<noscript><img src="' . $thumbMatch . '"/></noscript>';
                  echo '<img src="' . $thumbMatch . '"/>';
    
                } else {
    
                  echo '<span class="pending">';
                //echo '<a href="' .$name.'/'.$seed.'">Graphic</a> is ';
                  echo '<a href="index.php?src=' .$name.'&seed='.$seed.'" ';
                  echo 'id="'.$name.'">Graphic</a> is ';
                  echo '<a href="https://gitlab.com/chch/kombo/-/blob/main/mkko.sh"';
                  echo '   target="_blank">';
                  echo 'being built</a>. ';
                  echo 'Come back later!';
                  echo '</span>';
                  if (file_exists($pendingIcon)) {
                  echo '<img src="'.$pendingIcon.'" class="pending"/>';
                  }
                } 
    
                echo '<figcaption class="href">';
                echo '<div class="shorturl unveil"><a href="' . $baseUrl 
                                                              . '/list.php#' 
                                                              . $hrefid  
                                                              . '"></a></div>';
                echo '<div></div>';
                echo '<div></div>';
                if ( strlen($svgMatch) > 1 ) { echo '<div class="svgsrc">
                                                     <a href="' . $svgMatch
                                                                . '"></a></div>';
                } else { echo '<div></div>'; }
                echo '<div class="goto"><a href="' . $baseUrl
                                                   . '?src='
                                                   . $name 
                                                   . '&seed=' 
                                                   . $seed 
                                                   . '"></a></div>';
                echo '</figcaption>';
                echo '</figure></figure></figure>';
                echo "\n";
          }
         }
?>
</body></html>
