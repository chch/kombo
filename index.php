<?php // GLOBALS
      // -------------------------------------------------------------- //
         $ipconf = "ip.conf";
         if ( file_exists($ipconf) ) {
              $baseUrl = "http://" . trim(file($ipconf)[0]);
         } else {
              $baseUrl = "http://localhost:8000";
        }
        $srcDir  = "_";
        $outDir  = "s";
        $uiPath  = "_";

        $loadingSplash = $uiPath . "/" . "loadsplash.svg";
        $pinIcon = $uiPath . "/" . "pin.svg";
        $savedIcon = $uiPath . "/" . "save.svg";

        $nfoFile = "src.conf";
        $uiRef   = "01000000_0000000";

        $fields  = '^([0-9a-f]*)\|'    .            // $1 = checksum     
                   '([0-9a-f]{10})\|'  .            // $2 = srcbase
                   '([0-9a-f|]+)\|'    .            // $3 = layers
                   '([0-9]{3,})\|'     .            // $4 = seed
                   '([0-9]{13})$';                  // $5 = timestamp
?><?php 

      // DISPLAY HASH LIST (AS TXT LIST)
      // -------------------------------------------------------------- //
      // https://freeze.sh/_/2020/ioko/h (ashs)
         if ( isset( $_GET['do']) &&
                     $_GET['do'] == "lh" ) {

           if (isset( $_GET['mark'])){ $regMatch = "m"; }
                                else { $regMatch = "[0-9a-f]"; }
           if (isset( $_GET['all'])){ $regMatch = ""; }
           if (isset( $_GET['fp'])){ 

               $fingerprint = strip_tags(trim($_GET['fp']));
               if ( $regMatch == "m" ) {
                        $fpfile = $outDir . "/m" 
                                  . substr($fingerprint,1,31) 
                                  . ".list";
                        $regMatch = "/m" . substr($fingerprint,1,31);
               } else { $fpfile = $outDir . "/" 
                                  . $fingerprint 
                                  . ".list";
                        $regMatch = $fingerprint;
               }
               if ( file_exists($fpfile)) {
                    $regMatch = $regMatch; 
               } else { exit(0); }
           }

           foreach (glob($outDir."/".$regMatch."*.list")as $listPath) {
             $list = array_filter(explode("\n",
                                  file_get_contents($listPath)));
             foreach($list as $item) {
               echo explode('|',$item)[0] . "\n";
             }
           }

         exit(0);
         }
      // -------------------------------------------------------------- //
?><?php 

      // DISPLAY COLLECTED KOMBINATIONS (AS TXT LIST)
      // -------------------------------------------------------------- //
      // https://freeze.sh/_/2020/ioko/l (ist)
         if ( isset( $_GET['do']) &&
                     $_GET['do'] == "lk" ) {

           if (isset( $_GET['mark'])){ $regMatch = "m"; }
                                else { $regMatch = "[0-9a-f]"; }
           if (isset( $_GET['all'])){ $regMatch = ""; }
           if (isset( $_GET['fp'])){ 

               $fingerprint = strip_tags(trim($_GET['fp']));
               if ( $regMatch == "m" ) {
                        $fpfile = $outDir . "/m" 
                                  . substr($fingerprint,1,31) 
                                  . ".list";
                        $regMatch = "/m" . substr($fingerprint,1,31);
               } else { $fpfile = $outDir . "/" 
                                  . $fingerprint 
                                  . ".list";
                        $regMatch = $fingerprint;
               }
               if ( file_exists($fpfile)) {
                    $regMatch = $regMatch; 
               } else { exit(0); }
           }

           $unify = array();

           foreach (glob($outDir."/".$regMatch."*.list") as $listPath) {

              $list = array_values(array_filter(file($listPath),"trim"));

              // CREATE ARRAY FOR EACH HASH (=KEY)
              // OVERWRITE ITEM => ONE ENTRY FOR EACH HASH

              foreach($list as $key => $line) {

                $hash   = trim(preg_replace("/" . $fields . "/","$1",$line));
                $name   = trim(preg_replace("/" . $fields . "/","$2",$line));
                $layers = trim(preg_replace("/" . $fields . "/","$3",$line));
                $seed   = trim(preg_replace("/" . $fields . "/","$4",$line));
                $time   = substr($line,-14,13);
                $key    = $time . str_pad($key,10,"0",STR_PAD_LEFT);

                if (isset( $_GET['all'])){

                     $unify[$key] = array('hash'   => $hash,
                                          'name'   => $name, 
                                          'layers' => $layers,
                                          'seed'   => $seed,
                                          'time'   => $time);
                } else {

                    $unify[$hash] = array('hash'   => $hash,
                                          'name'   => $name, 
                                          'layers' => $layers,
                                          'seed'   => $seed,
                                          'time'   => $time);
                }
              }
            }

         // SELECT/SORT 
            $select = array();
            foreach($unify as $key => $item) {
               $hash   = $item['hash'];
               $name   = $item['name'];
               $layers = $item['layers'];
               $seed   = $item['seed'];
               $time   = $item['time'];
               $key    = $time .
                         str_pad($key,10,"0",STR_PAD_LEFT);
               $select[$key] = $hash . "|" . $name
                               . "|" . $layers . "|" . 
                               $seed . "|" . $time;
            }

            ksort($select,SORT_NUMERIC);

         // DISPLAY
            foreach ($select as $key => $val) {
                          echo $val . "\n";
            }

         }
      // -------------------------------------------------------------- //

?><?php 

      // WRITE KOMBINATION
      // -------------------------------------------------------------- //
      // https://freeze.sh/_/2020/ioko/w (rite)
         if ( isset( $_GET['do']) &&
                     $_GET['do'] == "w" &&
              isset( $_POST['stamp']) ) {

            if ( isset( $_POST['id']) ){
                 $fileName = md5(strip_tags(trim($_POST['id'])));
            } else {        
                 $fileName = md5("noidset");
            }

            if ( isset( $_GET['mark']) ) {

                 $fileName = "m" . substr($fileName,1,31);
            }

             $file = $outDir . "/" . $fileName  . ".list";

             $stamp = strip_tags(trim($_POST['stamp']));
             $name  = trim(preg_replace("/" . $fields . "/","$2",$stamp));
            $layers = trim(preg_replace("/" . $fields . "/","$3",$stamp));
           $default = preg_replace("/" . $fields . "/","$2$5",$stamp);

        $checkstore = preg_replace("/" . $fields . "/","$1",$stamp);
        $checkcheck = preg_replace("/(.)(.)(.)(.)(.)(.)(.)([|]*)/","$1$5$7",$layers);
        $split = str_split($checkcheck,3);sort($split);
        $checkcheck = substr($name,0,3) . implode($split);

         if ((strlen($default) == 23) &&
             //(!preg_match("/[^1-8b-e|]$/",$checkstore)) && // DEPENDENT ON mkui.sh
             (!preg_match("/^[^0-9a-f|]$/",$stamp)) &&
             ($checkcheck == $checkstore)
            ){ $f = fopen($file, "a"); // APPEND
               fwrite($f,$stamp . "\n");
               // Close the text file
               fclose($f);
             }
         }

      // -------------------------------------------------------------- //

?><?php 

      // DISPLAY KOMBO INTERFACE (= THE MAIN SITE)
      // -------------------------------------------------------------- //
         if ( !isset( $_GET['do']) ) {

      // -------------------------------------------------------------- //
         if ( isset($_SERVER['HTTP_REFERER']) ) {
          if ( strpos($_SERVER['HTTP_REFERER'],
                      preg_replace('/\/[0-9]*$/',
                      '',$_SERVER['REQUEST_URI'])) !== false ) {
               $preload = "no";
          } else { $preload = "yes"; }
         } else { $preload = "yes"; }
      // -------------------------------------------------------------- //
         if ( file_exists($nfoFile)) {

              $srcInfo = file($nfoFile);

      // -------------------------------------------------------------- //
         if ( isset( $_GET['src']) ) {

         $srcHash = strip_tags( trim(  $_GET[ 'src' ] ) );
         $srcPath = $srcDir . "/"  .$srcHash . ".css";

            if ( file_exists($srcPath)) {
   
              $selectSrc = $srcPath;
              $srcMatch  = array_values(
                           preg_grep("/^".$srcHash."/",$srcInfo))[0];
              $htmlTitle = preg_replace('/^[^:]*:[^:]*:/','',$srcMatch);
              $srcName   = preg_replace('/:.*$/','',
                           preg_replace('/^[^:]*:/','',$srcMatch));

            } else {

              $srcList   = glob($srcDir . '/*.css');
              $selectSrc = $nfoFile[array_rand($srcList)];
              $srcHash   = preg_replace('/\\.css$/','',basename($selectSrc));
              $srcMatch  = array_values(
                           preg_grep("/".$srcHash."/",$srcInfo))[0];
              $htmlTitle = preg_replace('/^[^:]*:[^:]*:/','',$srcMatch);
              $srcName   = preg_replace('/:.*$/','',
                           preg_replace('/^[^:]*:/','',$srcMatch));
            //header("Location: " . $baseUrl . "/" . $srcName);
              header("Location: " . $baseUrl . "/index.php?src=" . $srcHash);
            }

         } else {

           $nfoFile   = glob($srcDir . '/*.css');
           $selectSrc = $nfoFile[array_rand($nfoFile)];
           $srcHash   = preg_replace('/\\.css$/','',basename($selectSrc));
           $srcMatch  = preg_grep("/".$srcHash."/",$srcInfo);
           if ( !empty($srcMatch) ){ 
                $srcMatch = array_values($srcMatch)[0]; 
                $srcTitle  = preg_replace('/^[^:]*:[^:]*:/','',$srcMatch);
                $srcName   = preg_replace('/:.*$/','',
                             preg_replace('/^[^:]*:/','',$srcMatch));
              //header("Location: " . $baseUrl . "/" . $srcName);
                header("Location: " . $baseUrl . "/index.php?src=" . $srcHash);
           }
          }
         } 
      // -------------------------------------------------------------- //
      // FALLBACK IF CONFIGURATION IS MISSING
      // -------------------------------------------------------------- //
         if ( !isset($selectSrc) ) {

             $srcList = glob($srcDir . '/*.css');
             $selectSrc =  $nfoFile[array_rand($srcList)];

         }
         if ( !isset($htmlTitle) ) { $htmlTitle = ""; }
      // -------------------------------------------------------------- //
           $eID  = preg_replace('/\\.css$/','',basename($selectSrc));
           // NOT EFFICIENT AS file LOADS THE WHOLE BLOB (=TODO)
           $eNum = preg_replace('/[^0-9]*/','',file($selectSrc)[1]);
         $nthMax = preg_replace('/[^0-9]*/','',file($selectSrc)[3]);
          // TODO: BETTER + FALLBACK!!
      // -------------------------------------------------------------- //

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
<?php echo '<title>' . trim($htmlTitle) . '</title>' . "\n"; ?>
<meta name="viewport" content="width=device-width,user-scalable=no,
                               initial-scale=0.9,maximum-scale=0.9,
                               minimum-scale=0.9" />
<link rel="icon" href="data:;base64,iVBORw0KGgo=">
<style> @import "css/kombinator.css" </style>
<?php echo '<style> @import "' . $selectSrc . '" </style>' . "\n"; ?>
<style><?php if (file_exists($loadingSplash)) { 
             echo '#fullscreen.loading { background-image:url("'.$loadingSplash.'");}'."\n";
             }
             if (file_exists($pinIcon)) { 
             echo '#pin { background-image:url("'.$pinIcon.'"); }' . "\n";
             }
             if (file_exists($savedIcon)) { 
             echo '#saved { background-image:url("'.$savedIcon.'"); }' . "\n";
             }
       ?>
</style>
<script src="js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script>
$.fn.preload=function(){this.each(function(){ $('<img/>')[0].src = this;});}
$([<?php $comma = "";
         foreach (glob($srcDir . "/" . $eID  . "*.svg") as $svgFile) {
                  echo $comma . "'" . $svgFile . "'";
         $comma = ",";
      }
?>]).preload();
</script>
<script src="js/jquery.md5.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.mousewheel.js" type="text/javascript" charset="utf-8"></script>
<script src="js/seedrandom.js" type="text/javascript" charset="utf-8"></script>
<script src="js/client.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/hammer.js" type="text/javascript" charset="utf-8"></script>
</head>
<?php if ( $preload != "no" ) { ?>
<body class="loading">
<div id="fullscreen" class="loading"></div>
<?php } else  { ?>
<body>
<div id="fullscreen"></div>
<?php } ?>
<?php  $seedLength = $eNum * 3;
       if ( $seedLength < 13 ) { $seedLength = 13; }
       if ( isset( $_GET['seed']) ) {

            $seed = strip_tags( trim(  $_GET[ 'seed' ] ) );
            $startSeed = $seed;
         // ---------------------------------------------------------------- //
         // LOOK FOR SEED IN SAVED LISTS
         // --
         // 1. IF SEED IS IN LIST:      LET JS CHECK IF HASH MATCHES SEED
         // 2. IF SEED IS NOT IN LIST:  JUST USE IT, MIGHT BE WRONG
         // ---------------------------------------------------------------- //
          //$listUrl = $baseUrl . "/la";
         // https://stackoverflow.com/q/30120211
            $listUrl = http_build_query($baseUrl . '/index.php?do=lk&all');
            $list = array_values(array_filter(file($listUrl),"trim"));

            $matchSeed = array_values(preg_grep("/$seed/",$list));

            if ( !empty($matchSeed) ) { // FIND SEED MATCH

            $matchSeed = array_slice($matchSeed,-1)[0];
            $checksum  = trim(preg_replace("/" . $fields . "/","$1",$matchSeed));
            $seed      = trim(preg_replace("/" . $fields . "/","$4",$matchSeed));

            $matchChecksum = array_values(preg_grep("/$checksum/",$list));

            if ( !empty($matchChecksum) ) { // FIND MATCHING CHECKSUM (LAST ENTRY)

            $matchChecksum = array_slice($matchChecksum,-1)[0];

            $checksum  = trim(preg_replace("/" . $fields . "/","$1",$matchChecksum));
            $seed      = trim(preg_replace("/" . $fields . "/","$4",$matchChecksum));
            $layers    = trim(preg_replace("/" . $fields . "/","$3",$matchChecksum));

            $komboinit = "loadGrafik('" . $seed .
                                          "','"
                                        . $layers .
                                          "','"
                                        . $checksum .
                                          "');";
            }

            } else {

              // NO MATCHING SEED FOUND
              // -> CONFORM AND USE PROVIDED SEED
              $startSeed = substr($seed,0,$seedLength + 1);
              $komboinit = "switchGrafik('" . $startSeed . "','')";
            }
         // ---------------------------------------------------------------- //

       } else {   // NO SEED PROVIDED
                  // -> CREATE SEED
                  // echo "NO SEED PROVIDED";
                  $startSeed = substr( // MAKE SURE IT'S NOT TOO LONG
                                str_pad(preg_replace("/X/",mt_rand(0,9),
                                        preg_replace("/(.)/","$1X",
                                        mt_rand(100000000,999999999))),
                                        $seedLength,1),
                               0,$seedLength +  1);
                  $komboinit = "switchGrafik('" . $startSeed . "','')";
       }
?>
<div id="touchthis" unselectable="on" onselectstart="return false;"></div>
<div class="wrapper switch">
<?php 
      $eMaxCnt=array();
      $eMaxDone=array();
      $nthMaxCnt=array(); 
      foreach(glob($srcDir."/".$eID."*.svg") as $F) {

        if (!preg_match("/$uiRef/",$F)) { // IGNORE UI REF
  
            $eTypeFull = preg_replace("/(.*_[0-9]{8}_)/","",$F);
            if (!isset($eMaxDone[$eTypeFull])){ // NOT AFTER FIRST MATCH
             $eType = substr($eTypeFull,0,4);
             if (isset($eMaxCnt[$eType])){ // 
                 $eMaxCnt[$eType] = $eMaxCnt[$eType] + 1;
             } else { $eMaxCnt[$eType] = 1; }
            }
            if (isset($nthMaxCnt[$eTypeFull])){ // 
                 $nthMaxCnt[$eTypeFull] = $nthMaxCnt[$eTypeFull] + 1;
            } else { $nthMaxCnt[$eTypeFull] = 1; }
            $eMaxDone[$eTypeFull] = "DONE"; // MARKED AFTER FIRST MATCH 
        }
      }

      ksort($eMaxCnt,SORT_STRING);

      $nthMax = max($nthMaxCnt);
      for ($e = 1; $e <= $eNum; $e++) {
            echo '<div class="element e' . $e . '">';
            for ($n = 0; $n < $nthMax; $n++) {
                 echo '<div></div>';
            }
            echo '</div>' . "\n";
      }
?>
</div>
<div class="wrapper fixed"><div class="ref"><div></div></div></div>
<script type="text/javascript">
       var client = new ClientJS();
       var clientID = client.getFingerprint();
</script>
<script type="text/javascript">
<?php 
      echo "       var eID = \"" . $eID .           "\";\n";
      echo "      var eNum = \"" . $eNum .          "\";\n";
      echo "var globalSeed = \"" . $startSeed .     "\";\n";
      echo "   var srcName = \"" . trim($srcName) . "\";\n";

      $comma="";
      echo "      var eMax = [";
      foreach($eMaxCnt as $eMax){
       echo $comma . '"' . $eMax . '"';$comma=",";
      }
      echo "]". "\n";
?>
</script>
<script src="js/kombinator.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
var showurl = "<?php echo $baseUrl ?>/";
$(document).ready(function(){
<?php  echo $komboinit . "\n"   ?>
<?php if ( $preload != "no" ) { ?>
  setTimeout(function(){
<?php } ?>
  $("body").removeClass('loading');
  $("#fullscreen").removeClass('loading');
<?php if ( $preload != "no" ) { ?>
  },3000); // WAIT MILLISECONDS
<?php } ?>
});
</script>
<div id="pin" class="badge"><a href=""></a></div>
<div id="saved" class="badge"><a href=""></a></div>
<textarea id="pinurl" readonly="readonly" <?php 
 if ( $preload != "no" ) { ?>class="hide" <?php } ?> 
          onclick="this.focus();this.select()">
</textarea>
</body></html>
<?php } ?>
