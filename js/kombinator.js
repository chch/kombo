
   var kombi;var seed;
   var isDone;var isMarked;
   var eSet=[];var eLock=[];
   var selectMode = new Boolean(0);
   var keyHold = [];var lastKeyCode;
   var animationEnd = "animationend webkitAnimationEnd";
   var animationEnd =  animationEnd + "oAnimationEnd MSAnimationEnd";
   var panX;var panY;var pantime;
   var scroll = 0;var scrollSteps = 1;
   var switching = 0;

// ------------------------------------------------------------------------ // 
   if ( eID == "dev" ) {  savedList = [];
                         markedList = [];
   } else {               savedList = getList("index.php?do=lh");
                         markedList = getList("index.php?do=lh&mark="
                                               + $.md5(clientID));
   }
// ------------------------------------------------------------------------ // 
   var eSeq = new Array();
   for (var e = 0;e < eNum; e++) {

    thisMax = eMax[e];
    eSeq[e] = new Array();
    for (var s = 0;s <= 999; s++) {

      if ( thisMax != 1 ) { select = rndm(1,thisMax,s);
                            while ( select == eSeq[e][s-1] ) {
                                    select++;
                                    if (select > thisMax){ select = 1; }
                            }
      } else { select = 1;  }

     eSeq[e][s] = select;

    }

   if ( eMax[e] < 2 ) { eLock[e] = 2; }

   }
// ------------------------------------------------------------------------ // 
   function switchGrafik(seed,animation){

      seedSlit = seed;
      kombi="";kodes="";// RESET

      hidePinUrl();

      $('div.wrapper').css({ 'margin-left' : ''});
      $('div.ref').children('div')
                  .removeClass('movein fromLeft fromRight');
      $('div.element').children('div')
                      .removeClass('movein fromLeft fromRight');

      setTimeout(function(){

        $('div.ref').each(function(){
           $(this).children('div').removeClass();  
           $(this).children('div').addClass('movein ' + animation);
        });

        eCnt=0;$('div.element').each(function(){

            seedNow = seedSlit.substring(0,3);
           seedSlit = seedSlit.replace(/^[0-9]{3}/,"");

           if ( seedNow != '000' ) {

           selectE = eGet(eCnt,seedNow);
           classNew = "r" + selectE;

           $(this).children('div').removeClass();  
           $(this).children('div').addClass('movein ' + animation);
           $(this).children('div').addClass(classNew);
  
           eSet[eCnt] = 1;
           eName = $(this).children('div').first() //
                          .css('background-image') //
                          .replace(/\.svg.*$/g,"") //
                          .split(/_/);             //
           eName = eName[eName.length - 1];

           } else {

           eSet[eCnt] = 0;
           eName = "";
         //$(this).children('div').removeClass();  
           $(this).addClass("unSet");

           }

           if ( eName.match(/[a-f0-9]{6}/g) ) {

           kombi = kombi + "|" + eName; 
           kodes = kodes + "|" 
                   + eName.replace(/(.)(.)(.)(.)(.)(.)(.)/g,"$1$5$7");

           }  

        eCnt++;});

     // ----
        kombi = kombi.replace(/[\|]+/g,"|").replace(/^\|/g,"").replace(/\|$/g,"");
        kodes = kodes.replace(/[\|]+/g,"|").replace(/^\|/g,"").replace(/\|$/g,"");
     // ----
        if ( kodes == "" ) { checkMarked = 1;
                             $("#pin").hide();
                             checkSaved  = 1;
                             $("#saved").hide();
                             $('body').addClass("isDone");
        } else {

        // CREATE CHECKSUM BASED ON ELEMENT CODES
        // -------------------------------------- 
           checkstore = eID.substring(0,3) + // MAIN ID  REDUCED TO 3 CHARS 
                        kodes.split('|')     // SPLIT COLLECTION INTO ARRAY ...
                       .sort()               // ... SORT ...
                       .toString()           // ... BACK FROM ARRAY
                       .replace(/,/g,"")     // REMOVE SEPARATORS

           checkSaved = $.grep(savedList,function(e){
                         return e.match("^"+checkstore+"$");}).length;
          checkMarked = $.grep(markedList,function(e){
                         return e.match("^"+checkstore+"$");}).length;
        // ----
           $("#pin").show();
        // ----
           if ( checkSaved > 0 ) { isDone = 1;
                                   $("#saved").show();
                                   shortlink(kombi);
                                   $('body').addClass("isDone");
           } else { isDone = 0;
                    $('body').removeClass("isDone");
           }
        // ----
        }
     // --
        if ( checkMarked > 0 ) 
               { isMarked = 1;//console.log("IS MARKED");
        } else { isMarked = 0; }

    },10); // IT MAY BE ONLY 10 MILLISECONDS, IT'S ENOUGH

   }
// ------------------------------------------------------------------------ // 
   function saveGrafik() {

      if ( kodes != "" &&
           isDone != 1 ) {

        post = checkstore    //
               + "|" 
               + eID         //
               + "|" 
               + kombi       //
               + "|" 
               + globalSeed  //
               + "|" 
               + Date.now(); //

        post = post.replace(/[\|]+/g,"|");

        $('body').addClass("isDone");
         savedList.push(checkstore);
         isDone = 1;
         shortlink(kombi);

        if ( eID != "dev" ) { // DEV MODE

             $.ajax({
                url: "index.php?do=w",
                data: {stamp:post,id:clientID},
                datatype: "text",
                type: "POST",
              });
        }
      }
   }
// ------------------------------------------------------------------------ // 
   function markGrafik() {

      if ( kodes != "" &&
           isMarked != 1 ) {

        markedList.push(checkstore);

        post = checkstore    //
               + "|" 
               + eID         //
               + "|" 
               + kombi       //
               + "|" 
               + globalSeed  //
               + "|" 
               + Date.now(); //

        if ( eID != "dev" ) { // DEV MODE

             $.ajax({
                url: "index.php?do=w&mark",
                data: {stamp:post,id:clientID},
                datatype: "text",
                type: "POST",
              });
         }

         isMarked = 1;
      }

   }
// ------------------------------------------------------------------------ // 
   function loadGrafik(seed,layers,hash) {

       urlSeed = seed;
       seedSlit  = seed;
       layersMatch = layers;

       breakCnt=0;recalculated=false;// RESET


       kombi="";eName="";collectSeed="";kodes="";  // RESET

       eCnt=0;$('div.element').each(function(){

            eSet[eCnt] = 1;

            seedNow = seedSlit.substring(0,3);
         matchLayer = String(layersMatch).substring(0,7);
      // --------------------------------------------------------------- //
         while ( (matchLayer != eName) &&
                 (breakCnt < 999) ) {

           selectE = eGet(eCnt,seedNow);
           classNew = "r" + selectE;
           $(this).children('div').removeClass();
           $(this).children('div').addClass(classNew);
           eName = $(this).children('div').first()     //
                          .css('background-image')     //
                          .replace(/\.svg.*$/g,"")     //
                          .split(/_/);                 //
           eName = eName[eName.length - 1];

          // ------------------------------------------------------- //
             eType = eName.substring(0,3);
             eTypeMatch = matchLayer.substring(0,3);
             if ( eType != eTypeMatch) { lM2 = layers; }
          // ------------------------------------------------------- //
             while ( (eTypeMatch != eType) &&
                     (breakCnt < 999) ) { 
                      matchLayer = String(lM2).substring(0,7);
                      eTypeMatch = matchLayer.substring(0,3);
                      lM2 = lM2.replace(/^[a-f0-9]*\|/,"");
                      breakCnt++
             }
          // ------------------------------------------------------- //

           if ( eName != matchLayer ) {
            seedNow = parseInt(1 + seedNow,10) + 1;    // COUNT 
            seedNow = ('111' + seedNow).slice(-3);     // LIMIT
           }

           breakCnt++;

         }
      // --------------------------------------------------------------- //
/*       if ( matchLayer == eName ) { console.log((breakCnt)
                                      + ": " + matchLayer 
                                      + " == " + eName 
                                      + "  MATCH!");
         } else { console.log((breakCnt)
                  + ": " + matchLayer 
                  + " == " + eName 
                  + "  NO MATCH!"); }
*/
         if ( matchLayer != eName ) { seedNow = '000';
                                    //$(this).children('div').removeClass(); }
                                      $(this).addClass("unSet"); }

         if ( seedNow == '000' ) { eName = "";
                                   eSet[eCnt] = 0;                           
                                   seedSlit = seedSlit.replace(/^[0]{3}/,"");

         } else { seedSlit = seedSlit.replace(/^[0-9]{3}/,""); }

         // RM MATCHED LAYER FOR LOOP
         // https://stackoverflow.com/questions/494035
         mrx = new RegExp(matchLayer + "\\|","g"); 
         layersMatch = layersMatch.replace(mrx,"");

         kombi = kombi + "|" + eName;
         kodes = kodes + "|" 
                 + eName.replace(/(.)(.)(.)(.)(.)(.)(.)/g,"$1$5$7");

         if ( breakCnt > 1 ) { recalculated = true; }   // NEW SEED

         collectSeed = collectSeed + seedNow;           // COLLECT
         breakCnt = 0;                                  // RESET

       eCnt++;});

   // ----
      kombi = kombi.replace(/[\|]+/g,"|").replace(/^\|/g,"").replace(/\|$/g,"");
      kodes = kodes.replace(/[\|]+/g,"|").replace(/^\|/g,"").replace(/\|$/g,"");

   // CREATE CHECKSUM BASED ON ELEMENT CODES
   // -------------------------------------- 
      checkstore = eID.substring(0,3) + // MAIN ID  REDUCED TO 3 CHARS 
                   kodes.split('|')     // SPLIT COLLECTION INTO ARRAY ...
                  .sort()               // ... SORT ...
                  .toString()           // ... BACK FROM ARRAY
                  .replace(/,/g,"");    // REMOVE SEPARATORS

        checkSaved = $.grep(savedList,function(e){
                      return e.match("^"+checkstore+"$");}).length;
       checkMarked = $.grep(markedList,function(e){
                      return e.match("^"+checkstore+"$");}).length;
   // ----
      if ( checkSaved > 0 ) {
           $('body').addClass("isDone");
           isDone = 1;
           shortlink(kombi);
      } else {
           $('body').removeClass("isDone");
           isDone = 0;
      }
   // --
      if ( checkMarked > 0 ) 
             { isMarked = 1; //console.log("IS MARKED");
      } else { isMarked = 0; }
   // ----

      globalSeed = collectSeed + seedSlit; // COLLECT + ADD REMAINS

      if ( recalculated &&  urlSeed != globalSeed ) {

       //console.log("WAS RECALCULATED");
       setTimeout(function(){
        window.location.href = window.location.pathname
                              + "/" + globalSeed;
       },150); // WAIT A BIT

      }
   // ----
      showPinUrl("fixed");
   }
// ------------------------------------------------------------------------ // 
   function showPinUrl(state) {

           if ( state != "fixed" ) {
                $("textarea#pinurl").removeClass("hide");
           }
           $("textarea#pinurl").removeClass("moveout");
           $("textarea#pinurl").addClass(state);
           $("textarea#pinurl").val(showurl 
                                    + "index.php?src=" + eID 
                                    + "&seed=" + globalSeed);
   }
// ------------------------------------------------------------------------ // 
   function hidePinUrl() {

           $("textarea#pinurl").removeClass("fixed");
           $("textarea#pinurl").removeClass("movein");
           $("textarea#pinurl").addClass("moveout");
           $("textarea#pinurl").removeClass("focus");
           $( ".moveout" ).children().first().bind(animationEnd,
           function(){ $("textarea#pinurl").removeClass("hide");
            }
           );

   }
// ------------------------------------------------------------------------ // 
   function increase(){ slitscanCount(globalSeed,+1); }
   function decrease(){ slitscanCount(globalSeed,-1); }
// ------------------------------------------------------------------------ // 
   function slitscanCount(slitBase,operator) {

     seedSlit=slitBase;
     collectSeed="";eCnt=0; // RESET

     while ( seedSlit.length >= 3 ) {

        if ( eLock[eCnt] != '2' ) { // IF ELEMENT IS NOT LOCKED
         if ( eSet[eCnt] != '0' ) { // IF ELEMENT IS NOT UNSET

          seedNow = seedSlit.substring(0,3);             // EXTRACT

          if ( selectMode == true ) {

           if ( eLock[eCnt] == '1' ) {

            seedNow = Math.abs(parseInt(1 + seedNow,10)  // PREPARE
                                          + operator);   // COUNT 
            seedNow = ('111' + seedNow).slice(-3);       // PAD WITH 1s

           }
          } else {

            seedNow = Math.abs(parseInt(1 + seedNow,10)  // PREPARE
                                          + operator);   // COUNT 
            seedNow = ('111' + seedNow).slice(-3);       // PAD WITH 1s
          }

          if ( seedNow == '000' ) {                      // SKIP 000
          seedNow = Math.abs(parseInt(1 + seedNow,10)    // PREPARE
                                        + operator);     // COUNT 
          seedNow = ('111' + seedNow).slice(-3);         // PAD WITH 1s

          }
         } else { seedNow = '000'; }
        } else { seedNow = seedSlit.substring(0,3); }

        collectSeed = collectSeed + seedNow;          // COLLECT
        seedSlit = seedSlit.replace(/^[0-9]{3}/,"");  // REDUCE  

        eCnt++;

     }

     globalSeed = collectSeed + seedSlit; // COLLECTED + REMAINS

/*console.clear();
  console.log(globalSeed);*/

   }
// ------------------------------------------------------------------------ // 
   function rndm(min,max,seed) {

      min = Math.ceil(min);
      max = Math.floor(max);
      Math.seedrandom(seed);

      // http://lfkn.de/js-random 
      random =  Math.floor(Math.random()
                * (max - min + 1)) + min;

      return random;
   }
// ------------------------------------------------------------------------ // 
   function eGet(eCnt,seed) {

     return eSeq[eCnt][parseInt(seed,10)];

   }
// ------------------------------------------------------------------------ // 
   function getList(source) {

       REMOTE = source;
       rawFile = new XMLHttpRequest();
       rawFile.open("GET", REMOTE, false);
       rawFile.onreadystatechange = function ()
       { if(rawFile.readyState === 4)
         { if(rawFile.status === 200 || rawFile.status == 0)
           { loadFile  = rawFile.responseText.split('\n');
         }}}
       rawFile.send(null);
       return loadFile;

   }
// ------------------------------------------------------------------------ // 
   function shortlink(layerset) {

           hrefid = $.md5($.md5(layerset.replace(/^[\|]*/g,"")
                                        .replace(/[\|]*$/g,""))
                           .replace(/[^b-e4-8]/g,"")
                           .substring(0,8)).substring(0,5);
       // longurl = showurl + hrefid;
       //shorturl = "https://freeze.sh/k" + hrefid;
          longurl = showurl + "list.php#" + hrefid;

        $("#saved").children('a').prop("href", longurl)
   }
// ======================================================================== // 
// ------------------------------------------------------------------------ // 
   $(document).ready(function() {
  
           $('div.badge#pin').children('a').click(function(e) {
            e.preventDefault();
           });

           $( "#pin" ).children('a').click(function() {
                                     markGrafik();
                                     showPinUrl("movein");
             $(this).parent().addClass("clicked");
             }
           );

           $( "textarea#pinurl" ).click(function() {
                if ( $(this).hasClass("focus") ) {
                   //console.log("IS FOCUS");
                     setTimeout(function(){
                   //window.location.href = showurl + srcName
                   //                       + "/" + globalSeed;
                     window.location.href = showurl
                                            + "index.php?src=" + eID 
                                            + "&seed=" + globalSeed;

                     },150); // WAIT A BIT
                     $(this).removeClass("focus");
                } else {
                     $(this).addClass("focus");
                }
             }
           );

       $(".switch").on(animationEnd,
         function(e){ //console.log("SWITCHING FINISHED");
                      switching = 0;
                      $('div.element').css({'transform':''});
         }
       );
 
       eCnt=0;$('div.element').each(function(){
           if (eLock[eCnt]==2){$(this).addClass("isLocked");} 
       eCnt++;})
 
   });
// ------------------------------------------------------------------------ // 
// TOUCH CONTROL
// ------------------------------------------------------------------------ // 
   $(document).ready(function(){
// ------------------------------------------------------------------------ // 
   var hammeroptions = { dragLockToAxis: true,
                         dragBlockHorizontal: true,
                         preventDefault: true
                       };
    var touchthis = document.getElementById('touchthis');
    var mc = new Hammer(touchthis,hammeroptions);

  // listen to events...
  //mc.on("panleft panright tap press", function(ev) {
  //touchthis.textContent = ev.type +" gesture detected.";
  //});

 // ----------------------------------------------------------- //
    mc.on("panmove", function(e) {

     if ( switching != 1 ) {

      $('div.wrapper').css('margin-left',e.deltaX + 'px');

         if ( e.deltaX >  1 * ($(window).width()/2
                             - $(window).width()/5) ) {
              increase();
              switching = 1;
              switchGrafik(globalSeed,'fromLeft swipe');
         }

         if ( e.deltaX < -1 * ($(window).width()/2
                             - $(window).width()/5) ) {
               decrease();
               switching = 1;
               switchGrafik(globalSeed,'fromRight swipe');
         }
      }

    });
 // ----------------------------------------------------------- //
    mc.on("panmove", function(e) {

         if ( (isDone !=1) &&
              (e.deltaY > $(window).height()/100) ) {
              $('div.element').css({ 'transform' : ''});
              $("#fullscreen").addClass('isDone under');
              $("#fullscreen").css({'opacity':e.deltaY/500});;
         }
         if ( (isDone !=1) &&
              (e.deltaY > $(window).height()/2.5) ) {
              saveGrafik();
              $("#fullscreen").css({'opacity':''});;
              $("#fullscreen").removeClass('isDone under');
         }

    });
 // ----------------------------------------------------------- //
    mc.on("panstart", function(e) {
         //console.log("PAN STARTED");
           pantime = (new Date).getTime();
           panX = e.center.x;
           panY = e.center.y;
    });
 // ----------------------------------------------------------- //
    mc.on("panend", function(e) {
        //console.log("PAN ENDED");
          $('div.element').css({ 'transform' : ''});
          panX = panX - e.center.x;
          panY = panY - e.center.y;
          if ( (Math.abs(panX) > $(window).width()/20)
                && ((new Date).getTime() - pantime < 400 )
                && ( switching != 1 )) {
              if (Math.abs(panY) < $(window).height()/4) {
                //console.log("THIS IS CONSIDERED A SWIPE");
                  if ( panX > 0 ) {
                       decrease();
                       switching = 1;
                       switchGrafik(globalSeed,'fromRight swipe');
                  } else {
                       increase();
                       switching = 1;
                       switchGrafik(globalSeed,'fromLeft swipe');
                  }
              }
          } 
          $('div.wrapper').css({ 'margin-left' : ''});
          $("#fullscreen").css({'opacity':''});;
          $("#fullscreen").removeClass('isDone under');
    });
 // ----------------------------------------------------------- //
    mc.on("tap", function(e) {

     wW = $(window).width();
     wH = $(window).height();

        if ($("textarea#pinurl").hasClass("focus") ) {
  
             //console.log("TEXTAREA IS FOCUSSED");
               $("textarea#pinurl").removeClass("focus");
 
        } else {  if ( (e.center.y >  wH / 4) &&
                       (e.center.y <  wH
                                    - wH / 4) ) {
             
                       if ( e.center.x < wW / 2 
                                       - wW / 5 ) {
                            decrease();
                            switchGrafik(globalSeed,'fromRight swipe');
                       }  
                       if ( e.center.x > wW / 2 
                                       + wW / 5 ) {
                            increase();
                            switchGrafik(globalSeed,'fromLeft swipe');
                       }
                       if ((e.center.x < wW /  2 
                                       + wW / 12)  &&
                           (e.center.x > wW /  2
                                       - wW / 12)  && 
                           (e.center.y < wH /  2 
                                       + wH / 12) &&
                           (e.center.y > wH /  2
                                       - wH / 12) ) {
                            saveGrafik();
                       }
                  }
       }
   });
// ------------------------------------------------------------------------ // 
   });
// ------------------------------------------------------------------------ // 
// MOUSE CONTROL
// ------------------------------------------------------------------------ // 
   $(document).ready(function(){
// ------------------------------------------------------------------------ // 
   $(function() {
     $(window).mousewheel(function(event, delta) {

         if ( delta > 0 ) {

                  if ( Math.abs(scroll)%scrollSteps == 0 ) {
                       increase();
                       setTimeout(function(){
                        switchGrafik(globalSeed,'fromLeft mouse');
                       },150); // WAIT A BIT
                  }
                  scroll--; 
         } else { 
                  if ( Math.abs(scroll)%scrollSteps == 0 ) {
                       decrease();
                       setTimeout(function(){
                        switchGrafik(globalSeed,'fromRight mouse');
                       },150); // WAIT A BIT
                  }
                  scroll++;
         }
    });
   });

// ------------------------------------------------------------------------ // 
   });
// ------------------------------------------------------------------------ // 
// KEYBOARD CONTROL
// ------------------------------------------------------------------------ // 
   $(document).ready(function(){
// ------------------------------------------------------------------------ // 
    window.addEventListener("keydown", function(e) {
    // space and arrow keys and backspace
    if([32,37,38,39,40,8].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
   }, false);
// ------------------------------------------------------------------------ // 
   $(window).bind('keyup',function(e) {
     keyCode = e.keyCode || e.which;

     if ( keyCode == 39 ) {
          increase();
          setTimeout(function(){
           switchGrafik(globalSeed,'fromLeft arrowkey');
          },15); // WAIT A BIT
     }
     if ( keyCode == 37 ) {
          decrease();
          setTimeout(function(){
           switchGrafik(globalSeed,'fromRight arrowkey');
          },15); // WAIT A BIT
     }
     if ( keyCode == 32 && 
          selectMode == false ) {
          saveGrafik();
     }

   })
// ------------------------------------------------------------------------ // 
   var selectKeys = [ 49,50,51,52,53,54,55,56,57,48,81,87,69,82,84,90,85,73,
                      79,80,65,83,68,70,71,72,74,75,76,89,88,67,86,66,78,77 ];
// ----
   var eSelect=[];
   eCnt=0;$('div.element').each(function(){ 
      eSelect[selectKeys[eCnt]] = $(this);
   eCnt++;})
// ----
   // https://stackoverflow.com/questions/5203407
   onkeydown = onkeyup = function(e){
    e = e || event; // to deal with IE
    if(jQuery.inArray(e.keyCode,selectKeys) !== -1)
    { // LIMIT TO SELECT KEYS --------------------
   //setTimeout(function(){
      keyHold[e.keyCode] = e.type == 'keydown';
   //},5); // WAIT A BIT
    } // LIMIT TO SELECT KEYS --------------------
   }
// ----
   $(window).bind('keydown',function(e) {
     keyCode = e.keyCode || e.which;     
  // -----------------------------------------------------------
  /* LOCK ALL VISIBLE ELEMENTS + 
     MAKE VISIBLE/UNLOCK THE REST */
  // -----------------------------
     if ( keyCode == 190 ) { // PRESS COLON (:)

      eCnt=0;$('div.element').each(function(){

        if ( $(this).hasClass("unSet") ) {

          seedNow = rndm(100,999,Math.random());
          eSet[eCnt] = 1;
          eLock[eCnt]  = 1;
          $(this).removeClass("unSet");

        } else { eLock[eCnt] = 2;
                 $(this).addClass("isLocked");
        }

      eCnt++;});
     }
  // -----------------------------------------------------------
  // SWITCH/DELETE/LOCK ELEMENTS
  // --------------------------------
     if ( typeof eSelect[keyCode] != "undefined" ) {

      selectMode=true;

      thisKeyCode=keyCode;
      if ( thisKeyCode != lastKeyCode ) { 

      lastKeyCode = thisKeyCode;

      eCnt=0;$('div.element').each(function(){

       // ELEMENTS MATCHING KEY PRESS
       if ( keyHold[selectKeys[eCnt]] ) {

         if ( eLock[eCnt] != 2 ) {
         eLock[eCnt] = 1;
         }
         if ( eLock[eCnt] == 2 ) {
         $(this).addClass("isLocked");
         }

         $(this).addClass("select");
         $(this).removeClass("unselect");

       // OTHER ELEMENTS
       } else {

         if ( eLock[eCnt] != 2 ) {
         eLock[eCnt] = 0;
         $(this).addClass("unselect");
         $(this).removeClass("select");
         } else if ( eLock[eCnt] == 2 ) {
           $(this).removeClass("select");
           $(this).addClass("unselect isLocked");
         }
       }

      eCnt++;})

      }
     }
  // ---- LOCK ELEMENT
     if ( keyCode == 32 && // SPACE
          selectMode == true ) { 
  
      eLock.forEach(function(item,index){

       if ( eLock[index] == 1 ) {
            eLock[index]  = 2;
            $('div.element').eq(index).addClass("isLocked");
       } else if ( eLock[index] == 2 &&
                   $('div.element').eq(index).hasClass("select")) {
         eLock[index]  = 1;
         $('div.element').eq(index).removeClass("isLocked");
       }

      });
     }
  // ---- DELETE ELEMENT (=UNSET)
     if ( keyCode == 8 && // BACKSPACE
          selectMode == true ) { 

      seedSlit=globalSeed; // INIT
      collectSeed=""       // RESET

      eCnt=0;$('div.element').each(function(){
 
       seedNow = seedSlit.substring(0,3);           // EXTRACT

       // ELEMENTS MATCHING KEY PRESS
       if ( keyHold[selectKeys[eCnt]] ) {

        if ( $(this).hasClass("unSet") ) {

          seedNow = rndm(100,999,Math.random());
          eSet[eCnt] = 1;
          $(this).removeClass("unSet");

        } else { seedNow = '000';
                 eSet[eCnt] = 0;
                 $(this).addClass("unSet");
        }
       }

       collectSeed = collectSeed + seedNow;         // COLLECT
       seedSlit = seedSlit.replace(/^[0-9]{3}/,""); // REDUCE  

      eCnt++;})

     globalSeed = collectSeed;
     switchGrafik(globalSeed,'');

     }
  // -----------------------------------------------------------
   })
// ----
   $(window).bind('keyup',function(e){
     keyCode = e.keyCode || e.which;

     if ( typeof eSelect[keyCode] != 'undefined' ) {

      setTimeout(function(){

        if (jQuery.inArray(true,keyHold)==-1){selectMode=false;}

        eCnt=0;$('div.element').each(function(){

         if ( eLock[eCnt] != 2  &&
              !keyHold[selectKeys[eCnt]] ) {
              eLock[eCnt] = 0;
         } 

         if ( selectMode == true ) {
           $(this).removeClass("select unselect");
           if ( keyHold[selectKeys[eCnt]] ) {
                $(this).addClass("select");
           } else { $(this).addClass("unselect"); }
         } else { $(this).removeClass("select unselect"); }

        eCnt++;})

      },15); // WAIT A BIT

     }
     lastKeyCode = ""; // RESET
   })
// ------------------------------------------------------------------------ // 
   });
// ------------------------------------------------------------------------ // 

