#!/bin/bash

  WTHUMB="550";HTHUMB="300"
  OUTDIR="o";CONF="src.conf"

  if [ ! -f ip.conf ]
  then BASEURL="http://127.0.0.1:8000"
  else BASEURL=`hostname -I | sed 's/ //g'`":8000"
  fi;  CSSBASE="$BASEURL/_"
       KOMBOURL="$BASEURL/index.php?do=lk"
       ALLURL="$BASEURL/list.php"
  TMP="/tmp/MKIOKO"`echo "$0$*" | sed 's/ //g' | #
                    md5sum | cut -c 1-12       | #
                    awk '{ print toupper($0) }'`
# --------------------------------------------------------------------------- #
  SHDIR=`dirname \`realpath $0\``
  INKSCAPEEXTENSION="$SHDIR/lib/python"
  export PYTHONPATH="$INKSCAPEEXTENSION"
  APPLYTRANSFORMPY="$SHDIR/lib/python/applytransform.py"
  APPLYTRANSFORMPY=`realpath $APPLYTRANSFORMPY`
# --------------------------------------------------------------------------- #
  if [ ! -f "${TMP}.lock" ]
  then touch ${TMP}.lock;else >&2 echo "LOCKFILE EXISTS. EXITING.";exit 1;fi
# --------------------------------------------------------------------------- #
  SHDIR=`dirname \`realpath $0\``;cd $SHDIR
# --------------------------------------------------------------------------- #
  THUMBSCALEFLAG=`echo $* | sed 's/ /\n/g' | grep -- '--thumbscale=' | #
                  cut -d '=' -f 2- | sed 's/[^0-9\.]//g'`
  if [ "$THUMBSCALEFLAG" == "" ];then THUMBSCALEFLAG="1.0";fi
  if [ `echo $* | grep -- '--log' | wc -l` -gt 0 ];then LOG="YES";fi
  if [ `echo $* | grep -- '--quick' | wc -l` -gt 0 ];then MODUS="QUICK";fi
# --------------------------------------------------------------------------- #
# =========================================================================== #
# DUMP REMOTE STAMP LIST
# --------------------------------------------------------------------------- #
  wget --no-check-certificate -O ${TMP}.online $ALLURL   > /dev/null 2>&1
  wget --no-check-certificate -O ${TMP}.stamps $KOMBOURL > /dev/null 2>&1
# --------------------------------------------------------------------------- #
  if [ "$MODUS" == "QUICK" ];then SELECT="built";else SELECT=".";fi
# =========================================================================== #
  for SRCHASH in `grep $SELECT  ${TMP}.online          | #
                  sed 's/id="/\n&/g' | grep '^id=' | #
                  cut -d '"' -f 2 | cut -d '/' -f 1    | #
                  egrep '[0-9a-f]{10}' | sort -u`        #
  do
# --------------------------------------------------------------------------- #
# PROCESS STAMPS MATCHING $SRCHASH
# --------------------------------------------------------------------------- #
  for STAMP in  `cat ${TMP}.stamps     | # USELESS USE OF CAT
                 grep "|$SRCHASH|"     | #
                 sort -t "|" -k 1,1 -u | # SORT/UNIQ ACCORDING TO CHECKSUM
                 sort -t "|" -k 2,2 `     # SORT ACCORDING TO SRC HASH
   do  # --------------------------------------------------------------- #
       # EXTRACT KOMBINATION
       # --------------------------------------------------------------- #
       # SRCHASH=`echo $STAMP | cut -d "|" -f 2`
          LAYERS=`echo $STAMP            | # DISPLAY STAMP
                  cut -d "|" -f 3- | rev | # RM FIRST TWO FIELDS
                  cut -d "|" -f 3- | rev`  # RM LAST TWO FIELDS
            TIME=`echo $STAMP | rev | cut -d "|" -f 1 | rev`
       # --------------------------------------------------------------- #
         OUTID=`echo -n $LAYERS | md5sum | #
                sed 's/[^b-e4-8]//g'     | #
                cut -c 1-8`                #
  # ----------------------------------------------------------------------- #
    if [ "$MODUS" == "QUICK" ] &&
       [ `grep $OUTID ${TMP}.online | wc -l` -gt 0 ]
    then sleep 0 # DO NOTHING
  # ----------------------------------------------------------------------- #
    else
       # --------------------------------------------------------------- #
         UPDATE="" # RESET
       # ----
         DUMPBASE="${TMP}.${SRCHASH}"
         CSSURL="${CSSBASE}/${SRCHASH}.css";
         CSSDUMP="${DUMPBASE}.css";SVGDUMP="${DUMPBASE}.svg"
       # --------------------------------------------------------------- #
       # GET REMOTE FILES
       # --------------------------------------------------------------- #
       # DOWNLOAD ONCE
       # ----
         if [ ! -f "$CSSDUMP" ] 
          then wget --no-check-certificate \
               -O $CSSDUMP $CSSURL 2>&1 > /dev/null 2>&1
         fi
       # ---
       # SET VALUES OR SKIP
       # ---
         if [ -f $CSSDUMP ] 
          then SVGURL=`grep "SVGSRC" $CSSDUMP | #
                       head -n 1 | cut -d ":" -f 2- | #
                       sed 's/^[ ]*//'| cut -d " " -f 1`
               SRCNAME=`basename $SVGURL`
               SRCCONF=`grep $SRCNAME $CONF | #
                        cut -d "|" -f 2- | head -n 1`
               CONTINUE="YES"
          else >&2 echo "COULD NOT GET $CSSDUMP";CONTINUE="NO"
         fi
       # ---
       # GET FILE OR FAIL
       # ---
         if [ "$SVGURL" != "" ] && 
            [ "$CONTINUE" == YES ]
         then if [ ! -f "$SVGDUMP" ] # DOWNLOAD ONCE
              then  wget --no-check-certificate \
                    -O $SVGDUMP $SVGURL 2>&1 > /dev/null 2>&1
                    GITURL=`echo $SVGURL | sed 's|/raw/|/blob/|g'`
        LATESTHASH=`curl -s -L $GITURL         | # GET $GITURL
                    echo -e $(sed 's/%/\\x/g') | # lfkn.de/urldecode
                    sed 's/href=/\n&/g'        | # HREF ON NEW LINE
                    grep "^href="              | # GREP HREFS
                    cut -d '"' -f 2            | # BETWEEN '"'
                    egrep '[a-f0-9]{40}'       | # GREP HASH-LIKE
                    grep -- '-/commit'         | # GREP CORRECT HREF
                    tail -n 1                  | # SELECT LAST
                    sed 's/\(.*\)\([a-f0-9]\{40\}\)\(.*\)/\2/'`
            MD5SRC=`md5sum $SVGDUMP | cut -c 1-16`
          SRCSTAMP=`echo "<!-- $SRCNAME ($LATESTHASH/$MD5SRC) -->" | #
                    tr -s ' '`
                    sed -i "1s,^.*$,&\n$SRCSTAMP," $SVGDUMP
   # ----
     else SRCSTAMP=`grep '^<!-- .*\.svg ([0-9a-f]\+/[0-9a-f]\+) -->$' $SVGDUMP | #
                    head -n 1`
   # ----
               fi
         else >&2 echo '$SVGURL NOT SET. EXIT.';CONTINUE="NO"
         fi
       # --------------------------------------------------------------- #
         if [ ! -f $SVGDUMP ];then >&2 echo "COULD NOT GET $SVGURL. SKIP."
                                   CONTINUE="NO"
                              else CONTINUE="YES";fi
       # --------------------------------------------------------------- #
  # ------------------------------------------------------------------------- #
    B=N`echo ${SRCHASH} | sed 's/[^0-9]//g' | cut -c 1`L
    S=S`echo ${SRCHASH} | sed 's/[^0-9]//g' | cut -c 1`P
    L=L`echo ${SRCHASH} | sed 's/[^0-9]//g' | cut -c 1`O
  # ========================================================================= #
    if [ ! -f "${DUMPBASE}.layers" ] && [ "$CONTINUE" == YES ];then
  # ------------------------------------------------------------------------- #
  # PREPARE SOURCE SVG FOR PARSING (= PUT LAYERS ON SEPARATE LINES)
  # ------------------------------------------------------------------------- #
    sed ":a;N;\$!ba;s/\n/$B/g" $SVGDUMP   | # RM ALL LINEBREAKS (BUT SAVE)
    sed "s/ /$S/g"                        | # RM ALL SPACE (BUT SAVE)
    sed 's/<g/\n<g/g'                     | # REDO GROUP OPEN + NEWLINE
    sed "/mode=\"layer\"/s/<g/$L/g"       | # PLACEHOLDER FOR LAYERGROUP OPEN
    sed ':a;N;$!ba;s/\n//g'               | # RM ALL LINEBREAKS (AGAIN)
    sed "s/$L/\n<g/g"                     | # REDO LAYERGROUP OPEN + NEWLINE
    sed '/^[ ]*$/d'                       | # RM EMPTY LINES
    sed 's/<\/svg>/\n&/g'                 | # PUT SVG CLOSE ON NEW LINE
    sed 's/display:none/display:inline/g' | # MAKE VISIBLE EVEN WHEN HIDDEN
    grep -v "^</svg>"                     | # IGNORE SVG CLOSE
    tee > ${DUMPBASE}.layers                # WRITE TO TEMPORARY FILE
  # ----
    head -n 1 ${DUMPBASE}.layers > ${DUMPBASE}.head  # GET HEAD
    sed -i '1d' ${DUMPBASE}.layers                   # GET LAYERS (RM LINE 1) 
  # ------------------------------------------------------------------------- #
   (IFS=$'\n';CNT=1
    for LAYER in `cat ${DUMPBASE}.layers | grep "^<g"`
     do LAYERNAME=`echo $LAYER                     | # DISPLAY LAYER
                   sed  '/^<g/s/pe:label/\nlabel/' | # PUT NAME LABEL ON NL 
                   grep '^label' | cut -d "\"" -f 2` # EXTRACT NAME
         TYPEHASH=`echo $LAYERNAME      | # $LAYERNAME
                   sed 's/[-_][0-9]*$//'| # RM NUMBER
                   md5sum | cut -c 1-4`   # MAKE HASH
          TYPENUM=`echo $LAYERNAME                        | # $LAYERNAME
                   sed 's/\(.*\)\([-_]\)\([0-9]*$\)/\3/'  | # KEEP NUMBER
                   md5sum | sed 's/[^0-9]//g' | cut -c 1-3` # NUM ONLY HASH
        LAYERHASH="$TYPEHASH$TYPENUM"

        sed -i "${CNT}s/^/${LAYERHASH}:/" ${DUMPBASE}.layers
        CNT=`expr $CNT + 1`

    done;)
  # ------------------------------------------------------------------------- #
    fi
  # ========================================================================= #
  # DEFINE OUTPUT
  # ========================================================================= #
    LAYERGREP=`echo $LAYERS | sed 's/[0-9a-f]\{7\}/^&:/g'`
    LAYERNAMES=`egrep "$LAYERGREP" ${DUMPBASE}.layers   | #
                sed 's/:label=/\n&/g' | grep "^:label=" | #
                cut -d '"' -f 2 | sort -u`                #
  # ------------------------------------------------------------------------- #
       KOMBI=`echo $LAYERNAMES`
    BASENAME=`basename "$SRCNAME"`
      NID=`echo ${BASENAME}             | #
           tr '[:lower:]' '[:upper:]'   | #
           cut -d "-" -f 1              | #
           tr '[:lower:]' '[:upper:]'   | #
           md5sum | cut -c 1-4          | #
           tr -t '[:lower:]' '[:upper:]'` #
      FID=`echo $BASENAME               | #
           tr '[:lower:]' '[:upper:]'   | #
           md5sum | cut -c 1-4          | #
           tr '[:lower:]' '[:upper:]'`    #
      DIF=`echo ${KOMBI}.svg            | #
           md5sum | cut -c 1-9          | #
           tr '[:lower:]' '[:upper:]'   | #
           rev`                           #
      SVGNAME=`echo $NID$FID$DIF | cut -c 1-13`
    # =================================================================== #
    # CREATE SVG (TMP)
    # =================================================================== #
        # ----------------------------------------------------------- #
         cat ${DUMPBASE}.head                         >  ${TMP}.0.svg
         egrep "${LAYERGREP}" ${DUMPBASE}.layers | #
         cut -d ":" -f 2-                             >> ${TMP}.0.svg
         echo  "</svg>"                               >> ${TMP}.0.svg
        # ----
         grep -v ':label="XX_' ${TMP}.0.svg           >  ${TMP}.1.svg
        # ----
         sed -i "s/$S/ /g"                  ${TMP}.0.svg ${TMP}.1.svg
         sed -i "s/$B/\n/g"                 ${TMP}.0.svg ${TMP}.1.svg
         sed -i "/^[ \t]*$/d"               ${TMP}.0.svg ${TMP}.1.svg
        # ----------------------------------------------------------- #
    # ------------------------------------------------------------------- #
      if [ "$LOG" != "YES" ]
      then echo -e "\r\033[KCHECK:  ${SVGNAME}.svg ($SRCNAME)"
      fi
    # ------------------------------------------------------------------- #
      SVGOUT="${OUTDIR}/${SVGNAME}.svg"
      SHORTTIME=`echo $TIME | sed 's/\(.\)\(.\)/\2/g'`
      GIFOUT="$OUTDIR/${SVGNAME}_${OUTID}_${SHORTTIME}.gif"
    # ------------------------------------------------------------------- #
      if [ "$SVGNAME" != "ERROR" ] &&
         [ ! -f $SVGOUT ] || [ ! -f $GIFOUT ] ||
         [ `grep "$SRCSTAMP" $SVGOUT 2> /dev/null | wc -l` -lt 1 ]
      then
    # ------------------------------------------------------------------- #
          UPDATE="LOKAL"
        # ----------------------------------------------------------- #
        # MAKE IDs UNIQ
        # ----------------------------------------------------------- #
         (IFS=$'\n'
          for OLDID in `sed 's/id="/\n&/g' ${TMP}.1.svg | #
                        grep "^id=" | cut -d "\"" -f 2`
           do
              NEWID=`echo $SVGNAME$OLDID | md5sum | #
                     cut -c 1-9 | tr '[:lower:]' '[:upper:]'`
              sed -i "s,id=\"$OLDID\",id=\"$NEWID\",g" ${TMP}.1.svg
              sed -i "s,url(#$OLDID),url(#$NEWID),g"   ${TMP}.1.svg
          done; )
        # ----------------------------------------------------------- #
        # DO SOME CLEAN UP
        # ----------------------------------------------------------- #
          inkscape --vacuum-defs ${TMP}.1.svg > /dev/null 2>&1 # VACUUM CLEANER
          NLFOO=Nn${RANDOM}lL                                  # RANDOM PLACEHOLDER
          sed -i ":a;N;\$!ba;s/\n/$NLFOO/g" ${TMP}.1.svg       # FOR LINEBREAKS
        # ---
          cat ${TMP}.1.svg                        | # USELESS USE OF CAT
          sed "s,<defs,\n<defs,g"                 | #
          sed "s,</defs>,</defs>\n,g"             | #
          sed "/<\/defs>/!s/\/>/&\n/g"            | # SEPARATE DEFS
          sed "s,</sodipodi:[^>]*>,&\n,g"         | #
          sed "s,<.\?sodipodi,\nXXX&,g"           | #
          sed "/<\/sodipodi:[^>]*>/!s/\/>/&\n/g"  | # MARK TO RM SODIPODI
          sed "/^XXX.*/d"                         | # RM MARKED LINE
          tr -d '\n'                              | # DE-LINEBREAK (AGAIN)
          sed "s,<metadata,\nXXX&,g"              | #
          sed "s,</metadata>,&\n,g"               | #
          sed "/<\/metadata>/!s/\/>/&\n/g"        | # MARK TO RM METADATA
          sed "/^XXX.*/d"                         | # RM MARKED LINE
          sed "s/$NLFOO/\n/g"                     | # RESTORE LINEBREAKS
          sed "/^[ \t]*$/d"                       | # DELETE EMPTY LINES
          tee > ${SVGOUT}                           # WRITE TO FILE
        # ----------------------------------------------------------- #
    # ===================================================================== #
    # M A K E   T H U M B N A I L
    # ===================================================================== #
      BLACKFILL="#aaaaaa";BLACKSTROKE="#000000";OTHER="#cccccc"
    # ===================================================================== #
    # CREATE 2-BITISH THUMB
    # ===================================================================== #
      WSRC=`sed 's/ /\n/g' ${SVGOUT}     | #
            grep '^width=' | head -n 1   | #
            cut -d '"' -f 2`               #
      HSRC=`sed 's/ /\n/g' ${SVGOUT}     | #
            grep '^height=' | head -n 1  | #
            cut -d '"' -f 2`               # 
    # ----
      THUMBSCALE=`echo $SRCCONF          | #
                  sed 's/;/\n/g'         | #
                  grep "^thumbscale:"    | #
                  cut -d ":" -f 2        | #
                  sed '/[^0-9\.]\+/d'`     #
      if [ "$THUMBSCALE" == "" ];then THUMBSCALE="1.0";fi
      THUMBSCALE=`python -c "print $THUMBSCALE * $THUMBSCALEFLAG"`
      S=`python -c "print round((${HTHUMB}.0/$HSRC)*$THUMBSCALE,2)"`
    # ----
       WXTND=`python -c "print $WTHUMB * 1.5"`
       HXTND=`python -c "print $HTHUMB * 1.5"`
      XSHIFT=`python -c "print ($WXTND-$WSRC*$S)/2.0"`
      YSHIFT=`python -c "print ($HXTND-$HSRC*$S)/2.0"`
      TRANSFORMTRANSLATE="transform=\"translate($XSHIFT\,$YSHIFT)\""
      TRANSFORMSCALE="transform=\"scale($S\,$S)\""
      CANVAS="<path style=\"fill:#ccffaa;\" \
               d=\"m 0\,0 $WXTND\,0 0\,$HXTND -$WXTND\,0 z\" \
               id=\"c\" />"
    # ----
      TRANSFORM="<g $TRANSFORMTRANSLATE><g $TRANSFORMSCALE>"
    # ----
      sed -i "s/width=\"[0-9]*\"/width=\"$WXTND\"/g"    ${TMP}.0.svg
      sed -i "s/height=\"[0-9]*\"/height=\"$HXTND\"/g"  ${TMP}.0.svg
      sed -i -e ":a;N;\$!ba;s/\n/::N::/g"                   \
             -e "s/</\n</g" -e "s/<svg[^>]*>/&::TRNSFRM::/" \
             -e "s/::N::/\n/g"                          ${TMP}.0.svg
      sed -i 's,</svg>,</g></g>&,g'                     ${TMP}.0.svg
      sed -i "s,::TRNSFRM::,$CANVAS$TRANSFORM,g"        ${TMP}.0.svg
      sed -i "/^[ \t]*$/d"                              ${TMP}.0.svg
      SW="stroke-width"
      NEWSTROKE=`python -c "print round(1.2/$S,2)"`
      sed -i "s/$SW:[0-9\.]\+/$SW:$NEWSTROKE/g"         ${TMP}.0.svg
    # --------------------------------------------------------------------- #
      COUNT=1
      for COLOR in `sed 's/style="/\nS/g' ${TMP}.0.svg | #
                    sed 's/"/\n/g' | grep "^S"        | #
                    sed 's/#[0-9a-f]\{6\}/\n&\n/g'    | #
                    grep "^#" | sort -u`                #
       do sed -re "s/$COLOR/XxXxXx/g" ${TMP}.0.svg  | # PROTECT COLOR
          sed -re 's/#[0-9A-Fa-f]{6}/#ffffff/g'     | # ALL HEX TO WHITE
          sed -re "s/XxXxXx/#000000/g" > ${TMP}.1.svg # UNPROTECT TO BLACK
          inkscape --export-pdf=${TMP}.0.pdf ${TMP}.1.svg > /dev/null 2>&1
          convert -monochrome -flatten ${TMP}.0.pdf ${TMP}.0.gif
          convert ${TMP}.0.gif -fill $COLOR -opaque black ${TMP}.${COUNT}.gif
          if [ $COUNT -gt 1 ];then
          composite -compose Multiply -gravity center \
                     ${TMP}.collect.gif ${TMP}.${COUNT}.gif ${TMP}.0.gif
               mv ${TMP}.0.gif ${TMP}.collect.gif;rm ${TMP}.${COUNT}.gif
          else mv ${TMP}.${COUNT}.gif ${TMP}.collect.gif;rm ${TMP}.0.gif
          fi;  COUNT=`expr $COUNT + 1`
      done
      convert ${TMP}.collect.gif                          \
              +repage -background "#ccffaa"               \
              -gravity center -extent ${WTHUMB}x${HTHUMB} \
              -transparent "#ccffaa"                      \
              ${GIFOUT}
    # ---
      rm ${TMP}.[01].svg ${TMP}.0.pdf ${TMP}.collect.gif
    # ------------------------------------------------------------------- #
      fi
    # ------------------------------------------------------------------- #
  # ----------------------------------------------------------------------- #
    fi # QUICK MODE
 # ------------------------------------------------------------------------- #
  done
# =========================================================================== #
  done;# echo -e "\r\033[K"
# =========================================================================== #
# =========================================================================== #
# CLEAN UP (MAKE SURE $TMP IS SET FOR WILDCARD DELETE)
# =========================================================================== #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi

exit 0;
